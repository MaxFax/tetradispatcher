//
//  DispetcherNotificationController.m
//  Dispetcher
//
//  Created by Barbarossa on 15.08.14.
//
//

#import "DispetcherNotificationController.h"


@implementation DispetcherNotificationController

@synthesize cellType;
@synthesize myTableView;

#pragma mark - init
-(id)initWithTableView:(UITableView *)tableView
{
    self = [super init];
    if (self)
    {
        notifiArr=[[NSMutableArray alloc] init];
        self.cellType=@"notifiCell";
        self.myTableView=tableView;
        self.myTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        
        //Добавляем к таблице метку с информацией о том, что уведомлени нет
        CGRect parentFrame=myTableView.frame;
        CGRect labelFrame=CGRectMake(0,-150,parentFrame.size.width,parentFrame.size.height);
        noneNotifiLabel=[[UILabel alloc]initWithFrame:labelFrame];
        noneNotifiLabel.numberOfLines=0;
        noneNotifiLabel.lineBreakMode=NSLineBreakByWordWrapping;
        noneNotifiLabel.font=[UIFont systemFontOfSize:18.0];
        noneNotifiLabel.textAlignment=NSTextAlignmentCenter;
        noneNotifiLabel.text=@"Новых уведомлений нет";
        noneNotifiLabel.textColor=[UIColor grayColor];
        [myTableView addSubview:noneNotifiLabel];
    }
    return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

//Высота строк
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

//Количество строк
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (notifiArr.count==0)
    {
        noneNotifiLabel.hidden=NO;
        tableView.scrollEnabled=NO;
    }
    else
    {
        noneNotifiLabel.hidden=YES;
        tableView.scrollEnabled=YES;
    }
    return notifiArr.count;
}

//Отрисовка таблицы
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellType];
    cell.selectionStyle=UITableViewCellSelectionStyleDefault;
    notification=[notifiArr objectAtIndex:indexPath.row];
    
    UIImageView *imgNotifi=[[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
    
    UILabel *infoLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, 0, 125, 25)];
    UILabel *time=[[UILabel alloc]initWithFrame:CGRectMake(55, 23, 125, 35)];
    
    infoLabel.textColor=[UIColor blackColor];
    time.textColor=[UIColor blackColor];
    
    infoLabel.backgroundColor=[UIColor whiteColor];
    time.backgroundColor=[UIColor whiteColor];
    
    infoLabel.font=[UIFont systemFontOfSize:15.0f];
    time.font=[UIFont systemFontOfSize:14.0f];
    
    infoLabel.textAlignment=NSTextAlignmentLeft;
    time.textAlignment=NSTextAlignmentLeft;
    
    infoLabel.numberOfLines=0;
    time.numberOfLines=0;
    
    infoLabel.text=[notification valueForKey:@"info"];
    time.text=[NSDateFormatter localizedStringFromDate:[NSDate date]
                                             dateStyle:NSDateFormatterMediumStyle
                                             timeStyle:NSDateFormatterMediumStyle];

    
    int type=[[notification valueForKey:@"NotifiType"] intValue];
    switch (type)
    {
        case NotifiTypeTextMessage:
        {
            [imgNotifi setImage:[UIImage imageNamed:@"event1.png"]];
        }
            break;
        case NotifiTypeSystemMessage:
        {
            [imgNotifi setImage:[UIImage imageNamed:@"event2.png"]];
        }
            break;
        default:
            break;
    }
    [cell addSubview:infoLabel];
    [cell addSubview:time];
    [cell addSubview:imgNotifi];
    
    
    return cell;
}

//Выделение ячейки
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self tableView:myTableView
     commitEditingStyle:UITableViewCellEditingStyleDelete
     forRowAtIndexPath :indexPath];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [notifiArr removeObjectAtIndex:indexPath.row];
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
    }
}

#pragma mark - Methods

-(void)addNotificationWithDictionary:(NSDictionary *)dictionary
{
    [notifiArr insertObject:dictionary atIndex:0];
    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
    [myTableView beginUpdates];
    [myTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                       withRowAnimation:UITableViewRowAnimationTop];
    [myTableView endUpdates];
}
@end
