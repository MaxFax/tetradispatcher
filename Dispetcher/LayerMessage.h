//
//  LayerMessage.h
//  Dispetcher
//
//  Created by Admin on 25.06.14.
//
//

/**
 @memberof LayerMessage RectangleMessageDuration Длительность показа всплывающего сообщения<br>
 <ul>
 <li>kWTShort - Короткое сообщение</li>
 <li>kWTLong - Долгое сообщение</li>
 </ul>
 */
typedef NS_ENUM(NSInteger, RectangleMessageDuration)
{
    kWTShort = 1,
	kWTLong = 5
};

#import <QuartzCore/QuartzCore.h>

/**
 @brief Класс для отображения всплывающих уведомлений
 */
@interface LayerMessage : UIView

///Длительность отображения всплывающего уведомления
@property(nonatomic) NSInteger duration;

/**
 @method Статический метод для отображения уведомления
 @param (NSString *)text текст уведомления
 */
+ (void)showWithText:(NSString *)text;

/**
 @method Статический метод для отображения уведомления
 @param (NSString *)text текст уведомления
 @param (RectangleMessageDuration)duration длительность уведомления. Параметром передается объект из перечисления @ref RectangleMessageDuration.
 */
+ (void)showWithText:(NSString *)text duration:(RectangleMessageDuration)duration;

@end
