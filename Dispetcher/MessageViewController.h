//
//  MessageViewController.h
//  Dispetcher
//
//  Created by Admin on 04.07.14.
//
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import <CoreData/CoreData.h>
#import "SoundSDS.h"
#import "DispetcherNotificationController.h"
#import "MHProgressView.h"

@class ViewController;

/**
 @brief Представление для отображения диалога
 @details Интерфейс представления в котором реализуется графическая часть отправки и принятия сообщений Реализует набор методов и свойств следующих протоколов:
 <ul>
 <li>UITableViewDataSource,</li>
 <li>UITableViewDelegate,</li>
 <li>UITextFieldDelegate,</li>
 <li>UIActionSheetDelegate.</li>
 </ul>
 */
@interface MessageViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIActionSheetDelegate>
{
    ///Делегат главного контроллера вида
    ViewController *ViewControllerDelegat;
    NSTimer *loadTick;
}

///Свойство для доступа к делегату главного контроллера вида
@property (strong, nonatomic) ViewController *ViewControllerDelegat;

///свойство доступа к объекту, хранящему SSI текущего абонента
@property (strong, nonatomic) NSNumber *currentSubscriberSSI;
///свойство доступа к объекту, хранящему SSI абонента, которому отпраляются сообщения
@property (strong, nonatomic) NSNumber *targetSubscriberSSI;

///Свойство доступа к массиву, хранящему объекты, которые инкапсулируют в себе данные об абонентах.
@property (strong) NSMutableArray *messagesArr;

///Свойство доступа к текстовому полю ввода
@property (weak, nonatomic) IBOutlet UITextField *field;
///Метка, указывающая сколько символов осталось для ввода в поле
@property (strong, nonatomic) IBOutlet UILabel *lengthText;
///Индикатор, отображающийся при згрузке сообщений
@property (strong, nonatomic) IBOutlet MHProgressView *loadingProgressView;


///Нижняя панель с тектовым, содержащая @ref field, @ref lengthText, @ref loadingLabel
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
///Cвойство доступа к табличному представлению, куда добавляются сообщения.
@property (nonatomic, retain) IBOutlet UITableView *myDialogTable;
///Свойство для доступа к заголовку окна
@property (strong, nonatomic) IBOutlet UINavigationItem *navigationBar;
///Свойство для доступа к кнопке "Отправить"
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
///Свойство для доступа к кнопке "Почистить историю"
@property (strong, nonatomic) IBOutlet UIButton *clearButton;


/**@function Функция нажатию на кнопку @ref clearButton отчистки истории переписки
 @param (id)sender - параетр указывающий объект, вызвавший функцию;
 @return IBAction - указатель на действие выполняемое объектом
 */
- (IBAction)deleteMessage:(id)sender;
/** @function Функция нажатия на кнопку @ref sendButton - добавления исходящего сообщения на экран, а также его сохранении в CoreData
 @param (id)sender - параетр указывающий объект, вызвавший функцию;
 @return IBAction - указатель на действие выполняемое объектом
 */
- (IBAction)add:(id)sender;
/** 
 @method Функция добавления входящих сообщений на экран, а также его сохранении в CoreData
 @param (NSString*)message текст самого сообщения
 @param (NSNumber*)senderSSI SSI отправителя сообщения
 */
- (void) incomingSDSMessage:(NSString*)message fromSSI:(NSNumber*)senderSSI;
/** 
 @method Функция выбора SSI абенента, с которым будет вестиь диалог, переключение на нужное окно и перезагрузка массива сообщений из CoreData
 @param (NSNumber*)ssi SSI абонента, с которым будет вестись диалог
 */
-(void)selectTargetSubscriberSSI:(NSNumber*)ssi;

-(void)notDelivered;
-(void)wasDelivered;
@end
