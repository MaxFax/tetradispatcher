//
//  SubscriberTableViewCell.h
//  Dispetcher
//
//  Created by Barbarossa on 23.07.14.
//
//

#import <UIKit/UIKit.h>
#import "SVGKImageView.h"

@class SVGKImageView;

/**
 @brief Пользовательский класс для отображения ячеек в таблице со списком абонентов
 */
@interface SubscriberTableViewCell : UITableViewCell
{
    ///метка для отображения ssi абонента
    UILabel *ssiLabel;
    ///Метка для отображения наименования абонента
    UILabel *nameLabel;
    ///Метка для отображения количества непрочитанных сообщений от этого абонента
    UILabel *noticeLabel;
    ///ImageView для отображения иконки абонента
    SVGKImageView *subscrImage;
}
///@defgroup cellProp Свойства для обращения к меткам ячейки
///@{
@property (strong, nonatomic) IBOutlet UILabel *ssiLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *noticeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *subscrImage;
///@}
@end
