//
//  HistoryViewController.m
//  Dispetcher
//
//  Created by Admin on 01.07.14.
//
//

#import "HistoryViewController.h"
#import "CustomTableViewCell.h"
#import "HistoryDetailsController.h"
#import "AppDelegate.h"



@class CustomTableViewCell;

@implementation HistoryViewController
#pragma mark Propertyes
@synthesize tblHitory;
@synthesize labelDate,labelReceiver,labelSender,labelText;
@synthesize popoverContoller;
@synthesize myNavItem;


#pragma mark - init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    // Custom initialization
    }
    return self;
}

#pragma mark View lifecicle

- (void)viewDidLoad
{
    ///Подписка на уведомление о сообщениях
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getItems:)
												 name:@"showHistory"
                                               object:self.view.window];
    
    [super viewDidLoad];
    self.navigationItem.hidesBackButton=YES;
    
    labelDate.backgroundColor=[[UIColor alloc]initWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1];
    labelDate.layer.cornerRadius=8.0f;
    labelDate.layer.shadowRadius=8.0f;
    labelDate.text=NSLocalizedString(@"HISTORYDATE", nil);
    
    labelSender.backgroundColor=[[UIColor alloc]initWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1];
    labelSender.layer.cornerRadius=8.0f;
    labelSender.layer.shadowRadius=8.0f;
    labelSender.text=NSLocalizedString(@"HISTORYSENDER", nil);
    
    labelReceiver.backgroundColor=[[UIColor alloc]initWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1];
    labelReceiver.layer.cornerRadius=8.0f;
    labelReceiver.layer.shadowRadius=8.0f;
    labelReceiver.text=NSLocalizedString(@"HISTORYRESEIVER", nil);
    
    labelText.backgroundColor=[[UIColor alloc]initWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1];
    labelText.layer.cornerRadius=8.0f;
    labelText.layer.shadowRadius=8.0f;
    labelText.text=NSLocalizedString(@"HISTORYTEXTMESSAGE", nil);
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];

    self.navigationItem.leftBarButtonItem=delegate.myNavigationItem.leftBarButtonItem;
    self.navigationItem.rightBarButtonItem=delegate.myNavigationItem.rightBarButtonItem;
    
    tblHitory.delegate=self;
    tblHitory.dataSource=self;
    tblHitory.alwaysBounceVertical=NO;

}



-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    
    [self.navigationItem setTitleView:delegate.myNavigationItem.titleView];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationItem setTitleView:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TebleView methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return messages.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


///Метод для отображения обычных ячеек
/*
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellTable=@"Cell";
    
   UITableViewCell* myTableCell=[tableView dequeueReusableCellWithIdentifier:CellTable];
    if(!myTableCell)
    {
        myTableCell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
   
    
    myTableCell.textLabel.font=[UIFont boldSystemFontOfSize:14.0f];
    myTableCell.textLabel.text=@"Message";
    return myTableCell;
}
*/

///Метод для отображения custom ячеек
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellTable=@"myTableCell";
    
    CustomTableViewCell* myTableCell=(CustomTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellTable];
    if(!myTableCell)
    {
        ///Загружаем нашу собственную ячейку (custom cell)
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomTableViewCell"
                                                     owner:self
                                                   options:nil];
        myTableCell = [nib objectAtIndex:0];
    }
    
    ///Занимаемся заполнением строки таблицы (своей ячейки)
    NSManagedObject *mes=[messages objectAtIndex:indexPath.row];
    
    myTableCell.date.text=[NSDateFormatter localizedStringFromDate:[mes valueForKey:@"date"]
                                                         dateStyle:NSDateFormatterMediumStyle
                                                         timeStyle:NSDateFormatterMediumStyle];
    myTableCell.text.text=[mes valueForKey:@"text"];
    
    NSMutableSet *mutSet=[mes mutableSetValueForKey:@"receivers"];
    NSMutableString *receiversList=[[NSMutableString alloc] initWithString:@""];
    for(NSManagedObject *obj in mutSet)
    {
        [receiversList appendString:[[obj valueForKey:@"ssi"] stringValue]];
        if ([mutSet count]!=1)
        {
            [receiversList appendString:@", "];
        }
    }
    myTableCell.receiver.text=receiversList;
    
    myTableCell.sender.text=[[mes valueForKeyPath:@"sender.ssi"] stringValue];
    
    return myTableCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomTableViewCell *cell=(CustomTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    HistoryDetailsController *detailView=[[HistoryDetailsController alloc] initWithTextOfMessage:cell.text.text];

    popoverContoller=[[UIPopoverController alloc] initWithContentViewController:detailView];
    detailView.popoverController=self.popoverContoller;

    //self.popoverContoller.delegate=self;

    if (cell.text.text.length>32)
    {
        [popoverContoller presentPopoverFromRect:cell.bounds
                                          inView:cell.text
                        permittedArrowDirections:UIPopoverArrowDirectionUp
                                        animated:YES];
    }
}

#pragma mark - GUI

-(BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

-(void)getItems:(NSNotification *) paramNotification
{
    messages=[[paramNotification userInfo] valueForKey:@"messages"];
    [tblHitory reloadData];
}


/*

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Navigation NULL
@end
