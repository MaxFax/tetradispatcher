//
//  AppDelegate.h
//  Dispetcher
//
//  Created by Mac User on 1/3/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//


/**@mainpage Приложение Диспетчер, размещаемое на iPad. Предназначено для отправки и получения коротких сообщений и гео-данных от носимых терминалов, на которые подписан пользователь, а так же отображения этих терминалов на карте.
 @ref AppDelegate - класс-делегат приложения.<br>
 @ref ViewController - класс приложения с GUI и основной логикой программы.<br>
 @ref MessageViewController - GUI, отображающий диалоговое окно.<br>
 @ref HistoryViewController - GUI, отображающий историю всех сообщений.
 */



#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MessageViewController.h"
#import "HistoryViewController.h"

/**
 @brief Делегат приложения
 @details Содержит в себе @ref CoreDataStack, а также делегаты основных окон приложения.
 */
@interface AppDelegate : UIResponder <UIApplicationDelegate>

///Главное окно приложения
@property (strong, nonatomic) UIWindow *window;
///Контекст приложения для работы со средой CoreData
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
///Объект, описивающий модель хранения данных
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
///Объект-хранилище, специализированный под определенный тип хранилища данных (здесь SQLite)
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

///Делегат окна отображения диалога с абонентом
@property (strong, nonatomic) MessageViewController *myMessagesDelegate;
///Делегат окна, отображающего историю переписок со всеми абонентами
@property (strong, nonatomic) HistoryViewController *myHistoryDelegate;
///Делегат главного окна с картой и боковыми меню "Абоненты" и "Уведомления"
@property (strong, nonatomic) ViewController *myViewControllerDelegate;
///Делегат навигационной панели для ее отображения на всех контроллерах вида (во всех окнах).
@property (strong, nonatomic) UINavigationItem *myNavigationItem;

/**
 @method Для отправки изменений, сделанных приложением, обратно в хранилище данных.
 */
- (void)saveContext;
/**
 @function Функция, возвращающая директорию документов приложиния
 @return Путь к файлам приложения (используется для получения к файлу с БД SQLite)
 */
- (NSURL *)applicationDocumentsDirectory;
@end
