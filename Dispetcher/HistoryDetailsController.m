//
//  HistoryDetailsController.m
//  Dispetcher
//
//  Created by Barbarossa on 07.08.14.
//
//

#import "HistoryDetailsController.h"

@implementation HistoryDetailsController

@synthesize textOfMessage;
@synthesize popoverController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithTextOfMessage:(NSString *)text
{
    self = [super init];
    if (self) {
        // Custom initialization
        textOfMessage=[[UILabel alloc] init];
        textOfMessage.numberOfLines=0;
        textOfMessage.lineBreakMode=NSLineBreakByWordWrapping;
        textOfMessage.font=[UIFont systemFontOfSize:18.0];
        textOfMessage.textAlignment=NSTextAlignmentCenter;
        textOfMessage.textColor=[UIColor blackColor];
        [textOfMessage setText:text];
        [self.view addSubview:textOfMessage];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.contentSizeForViewInPopover=CGSizeMake(100.0f, 50.0f);
    
}

-(void)viewWillAppear:(BOOL)animated
{
    CGSize size=[textOfMessage.text sizeWithFont:[UIFont systemFontOfSize:18.0]
                 constrainedToSize:CGSizeMake(355.0f, 800.0f)
                     lineBreakMode:NSLineBreakByWordWrapping];
    size.width=355.0f;
    //size.height=100.0f;
   
    self.contentSizeForViewInPopover=CGSizeMake(size.width+20.0f,size.height+10.0f);
    self.textOfMessage.frame=CGRectMake(10, 2, size.width, size.height);
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
