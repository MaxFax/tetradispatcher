//
//  SoundSDS.m
//  Dispetcher
//
//  Created by Barbarossa on 13.08.14.
//
//

#import "SoundSDS.h"

@implementation SoundSDS

+(void)playSound:(NSString *)soundName
{
    SystemSoundID volleyFile;
    NSString *volleyPath = [[NSBundle mainBundle] pathForResource:soundName ofType:nil];
    CFURLRef volleyURL = (__bridge CFURLRef ) [NSURL fileURLWithPath:volleyPath];
    AudioServicesCreateSystemSoundID (volleyURL, &volleyFile);
    AudioServicesPlaySystemSound(volleyFile);
}

@end
