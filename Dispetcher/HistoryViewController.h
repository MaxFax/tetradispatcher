//
//  HistoryViewController.h
//  Dispetcher
//
//  Created by Admin on 01.07.14.
//
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ViewController.h"

/**
 @brief Класс представление для отображения таблицы со всей историей сообщений
 @details Контроллер вида для отображения всех, пришедших или полученных сообщений на данном утройстве в виде таблицы с заголовком @ref historyTableHeader. Реализует набор методов протоколов
 <ul>
    <li>UITableViewDataSource</li>,
    <li>UITableViewDelegate</li>,
    <li>UIPopoverControllerDelegate</li>.
 </ul>
 */
@interface HistoryViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>
{
    ///Массив всех сообщений на данном устройстве
    NSArray *messages;
    ///Контроллер выспылвающих сообщений
    UIPopoverController *popoverContoller;
}

///Свойство для обращения к навигационнному элементу
@property (strong, nonatomic) IBOutlet UINavigationItem *myNavItem;
///Свойство для обращения к табличному представлению
@property (strong, nonatomic) IBOutlet UITableView *tblHitory;
/**
 @defgroup historyTableHeader Свойства для обращению к полям заголовка таблицы
  @{
 */
///Надпись "Дата"
@property (strong, nonatomic) IBOutlet UILabel *labelDate;
///Надпись "Текст"
@property (strong, nonatomic) IBOutlet UILabel *labelText;
///Надпись "Отправитель"
@property (strong, nonatomic) IBOutlet UILabel *labelSender;
///Надпись "Получатели"
@property (strong, nonatomic) IBOutlet UILabel *labelReceiver;
@property (strong, nonatomic) UIPopoverController *popoverContoller;
///@}
///
/**
 @method Слушатель на обновление истории сообщений
 @param (NSNotification *) paramNotification словарь пользовательских данных содержащий информацию о сообщениях
 */
-(void)getItems:(NSNotification *) paramNotification;
//- (void)historyAction:(id)sender;
@end
