//
//  PopoverContentViewController.h
//  Dispetcher
//
//  Created by Mac User on 1/4/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "SVGKImageView.h"

@class ViewController;

/**
 @brief Контроллер вида для отображения меню приложения в виде всплывающего окна
 */
@interface PopoverContentViewController : UIViewController <UIAlertViewDelegate>
///Кнопка для обновления листа подключенных абонентов
@property(nonatomic,strong)UIButton *buttonRefresh;
///Кнопка выхода из приложения
@property(nonatomic,strong) UIButton *buttonExit;
///Делегат контроллера высплывающего сообщения
@property(nonatomic,weak) UIPopoverController *popoverController;
///Окно(?)
@property(nonatomic,strong)UIWindow *window;
///изображение рядом с кнопкой "Обновить список абонентов"
@property(nonatomic, strong)SVGKImageView *refreshImage;
///Изображение рядом с переключателем звука.
@property(nonatomic, strong)SVGKImageView *soundImage;
///Метка с заголовком переключателя звука
@property(nonatomic,strong)UILabel *backLabel;
///Переключатель, указывающий - воспроизводить звуки или нет
@property(nonatomic,strong) UISegmentedControl *soundsSwitch;
///Переменная, указывающая состояние переключателя
@property BOOL playSounds;
///Изображение рядом с информацией о текущем абоненте
@property(nonatomic, strong)SVGKImageView *informationImage;
///Метка с номером ssi текущего абонента
@property(nonatomic,strong)UILabel *ssiLabel;
///Метка с именем текущего абонента
@property(nonatomic,strong)UILabel *nameLabel;
///Изображения рядом с пунктом "Выход"
@property(nonatomic, strong)SVGKImageView *exitImage;
///Делегат главного контроллера вида
@property(nonatomic,strong)ViewController *viewDelegate;

@end
