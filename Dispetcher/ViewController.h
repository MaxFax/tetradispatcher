//
//  ViewController.h
//  DispetcherV2
//
//  Created by Barbarossa on 18.08.14.
//  Copyright (c) 2014 barbarossa.inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMMapView.h"
#import <MapKit/MapKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import "MessageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SubscriberTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocation.h>
#import "SimplePingHelper.h"
#import "DispetcherNotificationController.h"
#import "PopoverContentViewController.h"
#import "HistoryViewController.h"

#import "SVGKit.h"
#import "SVGKImageView.h"
#import "SVGKFastImageView.h"
#import "SVGKLayeredImageView.h"

/**
 @brief Интрефейс основоного функционала и взаиммодействия с пользователем
 @details Интерфейс представления в котором реализуется основная логика программы. Содержит графические элементы предствления и свойства и методы для работы с сокетом. Реализует набор методов и свойств следующих протоколов:
 <ul>
 <li>UITableViewDataSource,</li>
 <li>UITableViewDelegate,</li>
 <li>RMMapViewDelegate,</li>
 <li>UISearchDisplayDelegate,</li>
 <li>UIAlertViewDelegate,</li>
 <li>NSStreamDelegate</li>
 <li>CLLocationManagerDelegate.</li>
 </ul>
 */
@class MessageViewController;
@class HistoryViewController;
@class PopoverContentViewController;
@class SVGKFastImageView;
@class SVGKImageView;


@interface ViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,RMMapViewDelegate,UISearchDisplayDelegate,UIAlertViewDelegate,AVAudioPlayerDelegate,NSStreamDelegate,CLLocationManagerDelegate>
{
    ///SSI устройства, на котором запущено приложение
    NSNumber *currentSubscriberSSI;
    
    ///МАССИВ SSI ПОЛУЧЕННЫЙ ПО СЕТИ
    NSMutableArray *ssiList;
    
    ///Индекс последнего выбранного в таблице абонента
    NSIndexPath *clikIndex;
    ///Член, хранящий NSManagedObject объекты абонентов, загруженных из Core Data БД
    NSMutableArray *selectedSubscribers;
    ///Словарь счетчиков непрочитанных сообщений, где ключ - ssi терминала
    NSMutableDictionary *unreadMess;
    ///Словарь для хранения маркеров координат терминалов, где ключ - ssi терминала
    NSMutableDictionary *markersDictionary;
    
    ///Структура для хранения географических координат для вновь создаваемых маркеров.
    CLLocationCoordinate2D center;
    ///Маркер-менеджер для управления маркерами на RMMapView элементе GUI
    RMMarkerManager *markerManager;
    ///Член, инкапсулирующий маркер для размещения на карте
    RMMarker *newMarker;
    ///Таймер для проверки соединения с сервером
    NSTimer *reachabilityTimer;
    ///Входящий поток для TCP соединения с SEPURA
    NSInputStream *InputStream;
    ///Исходящий поток для TCP соединения с SEPURA
	NSOutputStream *OutputStream;
    ///Член, содержащий двоичные данные для отправки через исходящий поток
	NSMutableData *OutputData;
    ///Менеджер координат устройства
    CLLocationManager *gps;
    
    ///Входящий поток для TCP соединения с SEPURA
    NSInputStream *InputServerStream;
    ///Исходящий поток для TCP соединения с SEPURA
	NSOutputStream *OutputServerStream;
    
    ///Индикатор открыто ли боковое меню @ref mySubscribersTable
    BOOL isOpenSlideSubscribers;
    ///Индикатор открыто ли @ref myNotificationsTable
    BOOL isOpenSlydeNotifications;
    ///Индикатор включен ли Isolona сервер
    BOOL isOnlineServer;
    ///Контроллер представления меню приложения
    PopoverContentViewController *menuContent;
    ///Контроллер всплывающих сообщения, для отображения меню
    UIPopoverController *popoverController;
    ///Делегат контроллера уведомлений
    DispetcherNotificationController *notifiController;
    ///Делегат контролллера отображения истории сообщений @ref HistoryViewController
    HistoryViewController *historyController;
}


@property (strong, nonatomic) NSNumber *currentSubscriberSSI;

///Боковое меню Уведомления
@property (strong, nonatomic) IBOutlet UITableView *myNotificationsTable;
///Боковое меню Абоненты
@property (strong, nonatomic) IBOutlet UITableView *mySubscribersTable;

///Свойство для обращения к кнопке вызова меню
@property (strong, nonatomic) IBOutlet UIBarButtonItem *myMenuButton;
///Свойство для обращения к сегментному переключателю
@property (strong, nonatomic) IBOutlet UISegmentedControl *mySegmentMenu;
///Кнопка открытия/закрытия @ref myNotificationsTable
@property (strong, nonatomic) IBOutlet UIButton *mySlideNotificationsButton;
///Кнопка открытия/закрытия @ref mySubscribersTable
@property (strong, nonatomic) IBOutlet UIButton *mySlideSubscribersButton;
///Кнопка центрирования карты по текущим координатам устройства
@property (strong, nonatomic) IBOutlet UIButton *myCenterMapButton;

///Член, инкапсулирующий элемент GUI, представляющий графическую карту
@property (strong, nonatomic) IBOutlet RMMapView *myMapView;
///Контейнер содержащий в себе интерфейс диалога с абонентом
@property (strong, nonatomic) IBOutlet UIView *myMessagesContainer;
///Свойство для обращения к заголовку окна
@property (strong, nonatomic) IBOutlet UINavigationItem *myNavigationItem;
///Представление, содержащее в себе информационые элементы, отображающие состояние соединения с сервером
@property (strong, nonatomic) UIView *connectionStateView;

///Свойство для обращения к @ref gps
@property (strong, nonatomic) CLLocationManager *gps;
///Делегат контроллера, отображающего диалог с абонентом в контейнере @ref myMessagesContainer
@property (strong, nonatomic) MessageViewController *messagesController;
///Свойсто для обращения к @ref popoverController
@property (strong, nonatomic) UIPopoverController *popoverController;
///Свойсто для обращения к ячейке таблицы с абонентами
@property (strong, nonatomic) SubscriberTableViewCell *myTableCell;
///Делегат контроллера уведомлений @ref DispetcherNotificationController
@property (strong, nonatomic) DispetcherNotificationController *notifiController;
///Делегат контроллера, отображающего историю @ref HistoryViewController
@property (strong, nonatomic) HistoryViewController *historyController;


/** 
 @method Функция инициализации TCP соединения с модемом.
 */
-(void) TcpClientInitialise;
/**
 @method Метод для отправки текстового сообщения терминалу с указанным SSI по сокету TCP. Формирует командную строку с самим сообщением и помещает ее в исходящий поток.
 @param (NSString*)sds параметр с текстом сообщения
 @param (NSNumber*)targetSSI параметр с номером SSI абонента, которому предназначется сообщение
 */
- (void)sendSDSMessage:(NSString*)sds
                 toSSI:(NSNumber*)targetSSI;
/**
 @method Метод, занимающийся асинхронным прослушиванием сокета на предмет входящих сообщений. Обрабатывает их или перенаправляет по назначению.
 @param (NSStream *)theStream объект, для которого вызывается метод
 @param (NSStreamEvent)StreamEvent событие, которое обрабатывает метод
 */
- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)StreamEvent;

/**
 @function Функция для обработки нажатия на кнопку @ref mySegmentMenu
 @param (id)sender объект, который вызывает метод.
 @return IBAction - указатель на действие выполняемое объектом
 */
- (IBAction)segmentMenuSelected:(id)sender;
/**
 @function Функция для обработки нажатия на кнопку @ref mySlideSubscribersButton
 @param (id)sender объект, который вызывает метод.
 @return IBAction - указатель на действие выполняемое объектом
 */
- (IBAction)mySubscribersButtonClick:(id)sender;
/**
 @function Функция для обработки нажатия на кнопку @ref mySlideNotificationsButton
 @param (id)sender объект, который вызывает метод.
 @return IBAction - указатель на действие выполняемое объектом
 */
- (IBAction)myNotificationsButtonClick:(id)sender;
/**
 @function Функция для обработки нажатия на кнопку @ref myMenuButton
 @param (id)sender объект, который вызывает метод.
 @return IBAction - указатель на действие выполняемое объектом
 */
- (IBAction)showMenu:(id)sender;
@end
