//
//  MessageViewController.m
//  Dispetcher
//
//  Created by Admin on 04.07.14.
//
//

#import "MessageViewController.h"
#import "ViewController.h"
#import "AppDelegate.h"

@implementation MessageViewController
#pragma mark Свойства и константы

@synthesize currentSubscriberSSI;
@synthesize targetSubscriberSSI;

@synthesize myDialogTable;
@synthesize field;
@synthesize toolbar;
@synthesize messagesArr;
@synthesize ViewControllerDelegat;


@synthesize navigationBar;
@synthesize lengthText;
@synthesize loadingProgressView;
@synthesize sendButton,clearButton;

#define kBallonView 1
#define kLabel 2
#define kMessage 3
#define kDataTimeLabel 4

#pragma mark - init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    delegate.myMessagesDelegate=self;
    ViewControllerDelegat=delegate.myViewControllerDelegate;
    
    currentSubscriberSSI=[NSNumber numberWithLong:105];
    
    //GUI PREFERENCES
    myDialogTable.backgroundColor=[UIColor whiteColor];
    myDialogTable.layer.borderWidth=1.0;
    myDialogTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myDialogTable.delegate=self;
    myDialogTable.dataSource=self;
    myDialogTable.alwaysBounceVertical=NO;

    
    //Метка загрузки сообщений
    loadingProgressView.hidden=YES;
    loadingProgressView.progress=0;
    
    
    
    [field setDelegate:self];
    lengthText.text=@"250";
    lengthText.textColor=[UIColor grayColor];
    
    //Локализация строк
    [self.sendButton setTitle:NSLocalizedString(@"SENDMESSAGE", nil) forState:UIControlStateNormal];
    [self.clearButton setTitle:NSLocalizedString(@"CLEARHISTORY", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
												 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
}
- (void)viewWillDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark CoreData

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

///Выборка нужных сообщений
-(void) reloadMesArr
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Message"];
    
    NSSortDescriptor *sortByDate = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    [fetchRequest setSortDescriptors:[[NSArray alloc]initWithObjects: sortByDate, nil]];
    
    NSMutableArray *fetchResult=[[managedObjectContext executeFetchRequest:fetchRequest
                                                                     error:nil] mutableCopy];
    
    NSMutableArray *tempMessageArr=[[NSMutableArray alloc]init];
    for(NSManagedObject *mes in fetchResult)
    {
        if ([[mes valueForKeyPath:@"sender.ssi"] longValue]==[targetSubscriberSSI longValue])
            [tempMessageArr addObject:mes];
        
        NSMutableSet *set=[mes mutableSetValueForKey:@"receivers"];
        for(NSManagedObject *obj in set)
        {
            if ([[obj valueForKey:@"ssi"] longValue]==[targetSubscriberSSI longValue])
            {
                [tempMessageArr addObject:mes];
                break;
            }
        }
    }
    self.messagesArr = tempMessageArr;
    [self.myDialogTable reloadData];
}

- (void) deleteAllObjects: (NSString *) entityDescription  {
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSError *error;
    NSArray *items = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    
    for(NSManagedObject *mes in items)
    {
        NSMutableSet *set=[mes mutableSetValueForKey:@"receivers"];
        for(NSManagedObject *obj in set)
        {
            if ([[obj valueForKey:@"ssi"] longValue]==[targetSubscriberSSI longValue])
            {
                [managedObjectContext deleteObject:mes];
                break;
            }
        }
        if ([[mes valueForKeyPath:@"sender.ssi"] longValue]==[targetSubscriberSSI longValue])
        {
            [managedObjectContext deleteObject:mes];
        }
        
        
    }
    if (![managedObjectContext save:&error]) {
    	NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    [self reloadMesArr];
}

- (IBAction)deleteMessage:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:NSLocalizedString(@"DELETE", nil) otherButtonTitles:nil];
    [actionSheet showFromRect:[(UIButton *)sender frame] inView:self.view animated:YES];
    actionSheet.actionSheetStyle=UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
    [actionSheet setFrame:CGRectMake(0,0,100,100)];
    actionSheet.tag = 100;
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        [self deleteAllObjects:@"Message"];
    }
}


#pragma mark - TextField methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    lengthText.text=[NSString stringWithFormat:@"%d",249-[textField.text length]];
    
    return (newLength >= 250) ? NO : YES;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    lengthText.text=@"250";
    return YES;
}

#pragma mark TableView methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [messagesArr count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=@"Cell";
    
    UIImageView *baloonView;
    UILabel *label;
    UILabel *dateTimeLabel;
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        
        baloonView=[[UIImageView alloc]init];
        baloonView.tag=kBallonView;
        
        label=[[UILabel alloc]init];
        label.tag=kLabel;
        label.numberOfLines=0;
        label.lineBreakMode=NSLineBreakByWordWrapping;
        label.font=[UIFont systemFontOfSize:17.5];
        label.textAlignment=NSTextAlignmentLeft;
        
        dateTimeLabel=[[UILabel alloc]init];
        dateTimeLabel.tag=kDataTimeLabel;
        dateTimeLabel.numberOfLines=0;
        dateTimeLabel.lineBreakMode=NSLineBreakByWordWrapping;
        dateTimeLabel.font=[UIFont systemFontOfSize:17.5];
        dateTimeLabel.textAlignment=NSTextAlignmentLeft;
        //
        //dateTimeLabel.backgroundColor=[UIColor blueColor];
        
        UIView *message=[[UIView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
        message.tag=kMessage;
        [message addSubview:baloonView];
        [message addSubview:label];
        [message addSubview:dateTimeLabel];
        
        message.autoresizingMask=UIViewAutoresizingFlexibleWidth;
        
        [cell.contentView addSubview:message];
    }
    else
    {
        baloonView=(UIImageView *)[[cell.contentView viewWithTag:kMessage]viewWithTag:kBallonView];
        label=(UILabel *)[[cell.contentView viewWithTag:kMessage]viewWithTag:kLabel];
        dateTimeLabel=(UILabel*)[[cell.contentView viewWithTag:kMessage]viewWithTag:kDataTimeLabel];
    }
    
    NSManagedObject *dev = [self.messagesArr objectAtIndex:indexPath.row];
    NSString *text=[dev valueForKey:@"text"];
    NSManagedObject *sender=[dev valueForKey:@"sender"];
    
    CGSize size=[text sizeWithFont:[UIFont systemFontOfSize:17.5]
                 constrainedToSize:CGSizeMake(500.0f, 700.0f)
                     lineBreakMode:NSLineBreakByWordWrapping];
    UIImage *balloon;
    
    ///Проверка на входящее или исходящее сообщение
    if([[sender valueForKeyPath:@"ssi"] longValue]!=[currentSubscriberSSI longValue])
    {
        baloonView.frame=CGRectMake(0.0f, 0.0f, size.width+16.0f, size.height+16.0f);
        balloon=[[UIImage imageNamed:@"grey.png"]stretchableImageWithLeftCapWidth:24.0 topCapHeight:15.0];
        
        label.frame=CGRectMake(12.0f,4.0f, size.width, size.height);
        dateTimeLabel.frame=CGRectMake(15.0f,size.height+4.0f, 240.0f, 40.0f);
    }
    if([[sender valueForKeyPath:@"ssi"] longValue] == [currentSubscriberSSI longValue])
    {
        baloonView.frame=CGRectMake(842.0f-(size.width+32.0f), 0.0f, size.width+32.0f, size.height+16.0f);
        balloon=[[UIImage imageNamed:@"green.png"]stretchableImageWithLeftCapWidth:24.0 topCapHeight:15.0];
        
        label.frame=CGRectMake(842.0f-(size.width+16.0f),4.0f, size.width, size.height);
        dateTimeLabel.frame=CGRectMake(822.0f-(215),size.height+4.0f, 240.0f, 40.0f);
        
    }
    
    baloonView.image=balloon;
    label.text=text;
    NSString *strDateTime=[NSDateFormatter localizedStringFromDate:[dev valueForKey:@"date"]
                                                 dateStyle:NSDateFormatterMediumStyle
                                                            timeStyle:NSDateFormatterMediumStyle];
    
    //NSString *str2=[str stringByAppendingString:@"\u2713"];
    dateTimeLabel.text=strDateTime;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *dev = [self.messagesArr objectAtIndex:indexPath.row];
    NSString *text=[dev valueForKey:@"text"];
    CGSize size=[text sizeWithFont:[UIFont systemFontOfSize:24.0]constrainedToSize:CGSizeMake(500.0f, 700.0f) lineBreakMode:NSLineBreakByWordWrapping];
    return size.height+45.0f;
    
}



-(void)notDelivered
{
    
    // First figure out how many sections there are
    NSInteger lastSectionIndex = [myDialogTable numberOfSections] - 1;
    
    // Then grab the number of rows in the last section
    NSInteger lastRowIndex = [myDialogTable numberOfRowsInSection:lastSectionIndex] - 1;
    
    // Now just construct the index path
    NSIndexPath *pathToLastRow = [NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex];
    
    UITableViewCell *cell=[self.myDialogTable cellForRowAtIndexPath:pathToLastRow];
    if(cell!=nil)
    {
        UILabel *dateTimeLabel=(UILabel*)[[cell.contentView viewWithTag:kMessage]viewWithTag:kDataTimeLabel];
        NSString *str=dateTimeLabel.text;
        NSString *str2=[str stringByAppendingString:@" \u2718"];
        dateTimeLabel.text=str2;
    }
}

-(void)wasDelivered
{
    
    // First figure out how many sections there are
    NSInteger lastSectionIndex = [myDialogTable numberOfSections] - 1;
    
    // Then grab the number of rows in the last section
    NSInteger lastRowIndex = [myDialogTable numberOfRowsInSection:lastSectionIndex] - 1;
    
    // Now just construct the index path
    NSIndexPath *pathToLastRow = [NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex];
    
    UITableViewCell *cell=[self.myDialogTable cellForRowAtIndexPath:pathToLastRow];
    if(cell!=nil)
    {
        UILabel *dateTimeLabel=(UILabel*)[[cell.contentView viewWithTag:kMessage]viewWithTag:kDataTimeLabel];
        NSString *str=dateTimeLabel.text;
        NSString *str2=[str stringByAppendingString:@" \u2714"];
        dateTimeLabel.text=str2;
    }
}


#pragma mark - GUI methods





/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void) incomingSDSMessage:(NSString*)message
                    fromSSI:(NSNumber*)senderSSI
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *newMessageObj = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
    [newMessageObj setValue:message forKey:@"text"];
    [newMessageObj setValue:[NSDate date] forKey:@"date"];
    
    //Связываем сообщение с абонентами. получаем список абонентов.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Subscribers"];
    NSMutableArray *subscribers = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for(NSManagedObject *tempSubscriber in subscribers)
    {
        //Отправитель
        if ([[tempSubscriber valueForKey:@"ssi"] longValue]==[senderSSI longValue])
        {
            //Если это отправитель
            //              NSMutableSet *messageSender=[newDevice :@"sender"];
            //                [messageSender addObject:tempSubscriber];
            [newMessageObj setValue:tempSubscriber forKey:@"sender"];
            
        }
        //Получатель
        if ([[tempSubscriber valueForKey:@"ssi"] longValue]==[currentSubscriberSSI longValue])
        {
            //Если это получатель
            NSMutableSet *messageReceiver=[newMessageObj mutableSetValueForKey:@"receivers"];
            [messageReceiver addObject:tempSubscriber];
        }
    }
    //Сохраняем...
    NSError *error = nil;
    if(![context save:&error]){
        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
    }
    
    if([targetSubscriberSSI longValue]==[senderSSI longValue])
    {
        [self reloadMesArr];
        NSUInteger index = [messagesArr count] - 1;
        [myDialogTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    else
    {
        /*
        ///TODO
        ///В случае если не открыто окно далогов или ведется диалог с другим пользователем
        NSArray *obj=[[NSArray alloc]initWithObjects:[NSNumber numberWithInt:NotifiTypeTextMessage],[senderSSI stringValue], nil];
        NSArray *key=[[NSArray alloc]initWithObjects:@"NotifiType",@"info", nil];
        NSDictionary *dic=[[NSDictionary alloc]initWithObjects:obj forKeys:key];
        
        [self.notifiControllerDelegat addNotificationWithDictionary:dic];
         */
    }
}

- (IBAction)add:(id)sender {
    
    //Если поле не пустое
    if(![field.text isEqualToString:@""])
	{
        //Добавляем сообщение в CoreData
        NSManagedObjectContext *context = [self managedObjectContext];
        NSManagedObject *newMessage = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
        [newMessage setValue:field.text forKey:@"text"];
        [newMessage setValue:[NSDate date] forKey:@"date"];
        //Связываем сообщение с абонентами. получаем список абонентов.
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Subscribers"];
        //NSMutableArray *subscribers = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
        NSArray *subscribers = [context executeFetchRequest:fetchRequest error:nil];
        
        for(NSManagedObject *tempSubscriber in subscribers)
        {
            //Отправитель
            if ([[tempSubscriber valueForKey:@"ssi"] longValue]==[currentSubscriberSSI longValue])
            {
                //Если это отправитель
                [newMessage setValue:tempSubscriber forKey:@"sender"];
            }
            //Получатель
            if ([[tempSubscriber valueForKey:@"ssi"] longValue]==[targetSubscriberSSI longValue])
            {
                //Если это получатель
                NSMutableSet *messageReceiver=[newMessage mutableSetValueForKey:@"receivers"];
                [messageReceiver addObject:tempSubscriber];
            }
        }
        //Сохраняем...
        NSError *error = nil;
        if(![context save:&error]){
            NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
        }
        
        //Обновляем содержимое таблицы с собщениями и прокручиваем вниз к последнему сообщению
        [self reloadMesArr];
		NSUInteger index = [messagesArr count] - 1;
		[myDialogTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
        //Если сообщение отправляется не cамому себе,
        if ([currentSubscriberSSI longValue]!=[targetSubscriberSSI longValue])
        {
            //то отсылаем сообщение в сокет делегату главного контроллера вида.
            [self.ViewControllerDelegat sendSDSMessage:self.field.text
                                                 toSSI:targetSubscriberSSI];
        }
        //Звук сообщения
        //[SoundSDS playSound:@"example.caf"];
        
        //Конфигурация текстового поля
        lengthText.text=@"250";
		field.text = @"";
	}
    NSLog(@"add is done");
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    for (UIView *_currentView in actionSheet.subviews) {
        if ([_currentView isKindOfClass:[UIButton class]]) {
            [((UIButton *)_currentView).titleLabel setFont:[UIFont boldSystemFontOfSize:18.f]];
        }
    }
}





#pragma mark delegat methods

-(void)selectTargetSubscriberSSI:(NSNumber*)ssi
{
    if ([targetSubscriberSSI longValue] !=[ssi longValue])
    {
        targetSubscriberSSI=ssi;
        [self.navigationBar setTitle:[targetSubscriberSSI stringValue]];
        ///Перезагружаем данные истории сообщений
        CGFloat stop=0;
        loadingProgressView.progress=stop;
        //CGFloat var=loadingProgressView.progress;
        //NSLog(@"Loading Starts %f",var);
        loadingProgressView.hidden=NO;
        
        [self loadingAnimation];
    }
}

-(void)loadingAnimation
{
    loadingProgressView.progress = loadingProgressView.progress+0.15;
    CGFloat var=loadingProgressView.progress;
    //NSLog(@"Loading %f",var);

    CGFloat stop=1.200000;
    if(var>stop)
    {
        loadingProgressView.hidden=YES;
        stop=0;
        loadingProgressView.progress=stop;
        [self reloadMesArr];
    }
    else
    {
        [self performSelector:@selector(loadingAnimation) withObject:nil afterDelay:0.1];
    }
}

- (void)keyboardWillShow:(NSNotification *)paramNotification {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
	toolbar.frame = CGRectMake(42, 300, 895, 44);
    lengthText.frame=CGRectMake(13, 311, 42, 21);
    myDialogTable.frame=CGRectMake(0, 0, 844, 300);
	[UIView commitAnimations];
	
	if([messagesArr count] > 0)
	{
		NSUInteger index = [messagesArr count] - 1;
		[myDialogTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
	}
}

//Устаревшаю функция
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    //NSLog(@"Return YES\n");
	[self.field resignFirstResponder];
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
	toolbar.frame = CGRectMake(42, 661, 895, 44);
    lengthText.frame=CGRectMake(13, 672, 42, 21);
    myDialogTable.frame=CGRectMake(0, 0, 844, 661);
	[UIView commitAnimations];
	
	return YES;
}
///Анимация клавиатуры

-(void)textFieldDidEndEditing:(UITextField *)textField
{
	[self.field resignFirstResponder];
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
	toolbar.frame = CGRectMake(42, 661, 895, 44);
    lengthText.frame=CGRectMake(13, 672, 42, 21);
    myDialogTable.frame=CGRectMake(0, 0, 844, 661);
	[UIView commitAnimations];
}

@end
