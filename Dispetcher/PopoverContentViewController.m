//
//  PopoverContentViewController.m
//  Dispetcher
//
//  Created by Mac User on 1/4/14.
//  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
//

#import "PopoverContentViewController.h"

@implementation PopoverContentViewController

@synthesize buttonRefresh,buttonExit,popoverController,window=_window,viewDelegate,refreshImage, soundImage, backLabel, soundsSwitch,  informationImage, ssiLabel, nameLabel, exitImage;
@synthesize playSounds;




#pragma mark init
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Окно поповера
    self.view.backgroundColor=[UIColor whiteColor];
    //Изображение обновления
    CGRect refrashRect=CGRectMake(15.0f, 25.0f, 25.0f, 25.0f);
    self.refreshImage=[[SVGKFastImageView alloc]initWithSVGKImage:[SVGKImage imageNamed:@"menu1.svg"]];
    self.refreshImage.frame=refrashRect;
    [self.view addSubview:self.refreshImage];
    //Кнопка обновления
    CGRect buttonRect=CGRectMake(40.0f, 27.0f, 220.0f, 25.0f);
    self.buttonRefresh=[UIButton buttonWithType:UIButtonTypeCustom];
    [self.buttonRefresh setTitle:NSLocalizedString(@"UPDATELIST", nil) forState:UIControlStateNormal];
    [self.buttonRefresh setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    buttonRefresh.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
/*
    [self.buttonRefresh addTarget:self action:@selector() forControlEvents:UIControlEventTouchUpInside];
 */
    self.buttonRefresh.backgroundColor=[UIColor whiteColor];
    self.buttonRefresh.frame=buttonRect;
    [self.view addSubview:self.buttonRefresh];
    //Разделительная линия
    UIView *line1=[[UIView alloc] initWithFrame:CGRectMake(10.0f, 54.0f, 253.0f, 1.0f)];
    line1.backgroundColor = [UIColor lightGrayColor];
    line1.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    line1.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    line1.layer.shadowRadius = 0.5f;
    line1.layer.shadowOpacity = 0.4f;
    line1.layer.masksToBounds =NO;
    [self.view addSubview:line1];
    //Изображение звука
    CGRect languageRect=CGRectMake(15.0f, 64.0f, 25.0f, 25.0f);
    self.soundImage =[[SVGKFastImageView alloc]initWithSVGKImage:[SVGKImage imageNamed:@"menu2.svg"]];
    self.soundImage.frame=languageRect;
    [self.view addSubview:self.soundImage];
    //Заголовок вкл/выкл звука
    CGRect backRect=CGRectMake(50.0f, 64.0f, 200.0f, 25.0f);
    self.backLabel=[[UILabel alloc]initWithFrame:backRect];
    self.backLabel.font= [UIFont fontWithName:@"Helvetica" size:15];
    self.backLabel.text=NSLocalizedString(@"SOUND", nil);
    [self.view addSubview:self.backLabel];
    //Переключение вкл/выкл звука
    NSArray *itemArray = [NSArray arrayWithObjects:NSLocalizedString(@"ON", nil),NSLocalizedString(@"OFF", nil), nil];
    soundsSwitch= [[UISegmentedControl alloc] initWithItems:itemArray];
    soundsSwitch.frame = CGRectMake(50.0f, 90.0f, 180, 28);
    [soundsSwitch addTarget:self action:@selector(soundSwitchAction:) forControlEvents: UIControlEventValueChanged];
    soundsSwitch.selectedSegmentIndex = 0;
    soundsSwitch.tintColor=[UIColor purpleColor];
    [self.view addSubview:self.soundsSwitch];
    //Разделительная линия
    UIView *line2=[[UIView alloc] initWithFrame:CGRectMake(10.0f,125.0f, 253.0f, 1.0f)];
    line2.backgroundColor = [UIColor lightGrayColor];
    line2.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    line2.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    line2.layer.shadowRadius = 0.5f;
    line2.layer.shadowOpacity = 0.4f;
    line2.layer.masksToBounds =NO;
    [self.view addSubview:line2];
    //Иконка информация
    CGRect informationRect=CGRectMake(15.0f, 135.0f, 25.0f, 25.0f);
    self.informationImage =[[SVGKFastImageView alloc]initWithSVGKImage:[SVGKImage imageNamed:@"menu3.svg"]];
    self.informationImage.frame=informationRect;
    [self.view addSubview:self.informationImage];
    //Отображение SSI пользователя
    CGRect ssiRect=CGRectMake(50.0f, 135.0f, 120.0f, 20.0f);
    self.ssiLabel=[[UILabel alloc]initWithFrame:ssiRect];
    self.ssiLabel.font= [UIFont fontWithName:@"Helvetica" size:15];
    self.ssiLabel.text=@"205";
    [self.view addSubview:self.ssiLabel];
    //Отображение имени пользователя
    CGRect nameRect=CGRectMake(50.0f, 158.0f, 120.0f, 50.0f);
    self.nameLabel=[[UILabel alloc]initWithFrame:nameRect];
    self.nameLabel.font= [UIFont fontWithName:@"Helvetica" size:15];
    self.nameLabel.text=@"Давыдов Давид Давидович";
    self.nameLabel.textAlignment =  NSTextAlignmentLeft;
    self.nameLabel.numberOfLines=0;
    [self.nameLabel sizeToFit];
    [self.view addSubview:self.nameLabel];
    //Разделительная линия
    UIView *line3=[[UIView alloc] initWithFrame:CGRectMake(10.0f,218.0f, 253.0f, 1.0f)];
    line3.backgroundColor = [UIColor lightGrayColor];
    line3.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    line3.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    line3.layer.shadowRadius = 0.5f;
    line3.layer.shadowOpacity = 0.4f;
    line3.layer.masksToBounds =NO;
    [self.view addSubview:line3];
    //Изображения выхода из приложения
    CGRect exitRect=CGRectMake(15.0f, 228.0f, 25.0f, 25.0f);
    self.exitImage =[[SVGKFastImageView alloc]initWithSVGKImage:[SVGKImage imageNamed:@"menu4.svg"]];
    self.exitImage.frame=exitRect;
    [self.view addSubview:self.exitImage];
    //Кнопка выхода из приложения
    CGRect buttonExitRect=CGRectMake(50.0f, 228.0f, 205.0f, 45.0f);
    self.buttonExit=[UIButton buttonWithType:UIButtonTypeCustom];
    [self.buttonExit setTitle:NSLocalizedString(@"EXIT", nil) forState:UIControlStateNormal];
    [self.buttonExit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.buttonExit.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    self.buttonExit.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    self.buttonExit.contentVerticalAlignment=UIControlContentVerticalAlignmentTop;

    [self.buttonExit addTarget:self
                        action:@selector(pressExitButton:)
              forControlEvents:UIControlEventTouchUpInside];
    self.buttonExit.frame=buttonExitRect;
    [self.view addSubview:self.buttonExit];
    
    playSounds=YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    //self.buttonMaps=nil;
    //self.buttonHistory=nil;
    self.popoverController=nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
#pragma mark Methods

-(void)soundSwitchAction:(id)paramSendeer
{
    if (self.playSounds)
    {
        self.playSounds=NO;
    }
    else
    {
        self.playSounds=YES;
    }
}

-(NSString *) yesButtonTitle
{
    return NSLocalizedString(@"YES", nil);
}

-(NSString *) noButtonTitle
{
    return NSLocalizedString(@"NO", nil);
}


-(void) pressExitButton:(id)paramSender
{
    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:nil message:@"Выйти?" delegate:self cancelButtonTitle:[self noButtonTitle] otherButtonTitles:[self yesButtonTitle], nil];
    [alertView show];
    if([self isInPopover])
    {
        [self.popoverController dismissPopoverAnimated:YES];
    }
}


-(void) gotoHistory:(id)paramSender
{
    //    [self.viewDelegate gotoHistoryViewController:paramSender];
    [self.popoverController dismissPopoverAnimated:YES];
}

-(void) gotoMaps:(id)paramSender
{
    //    [self.viewDelegate gotoMapsViewController:paramSender];
    [self.popoverController dismissPopoverAnimated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle=[alertView buttonTitleAtIndex:buttonIndex];
    if([buttonTitle isEqualToString:[self yesButtonTitle]])
    {
        exit(0);
    }
    else if ([buttonTitle isEqualToString:[self noButtonTitle]])
    {
        NSLog(@"User pressed the No button");
    }
}

-(BOOL)isInPopover
{
    Class popoverClass=NSClassFromString(@"UIPopoverController");
    if(popoverClass!=nil && UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad && self.popoverController !=nil)
    {
        return YES;
    }else
    {
        return NO;
    }
}
@end
