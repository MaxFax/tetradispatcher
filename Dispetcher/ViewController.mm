//
//  ViewController.m
//  DispetcherV2
//
//  Created by Barbarossa on 18.08.14.
//  Copyright (c) 2014 barbarossa.inc. All rights reserved.
//

#import "ViewController.h"
#import "RMMapContents.h"
#import "RMFoundation.h"
#import "PopoverContentViewController.h"
#import "RMDBMapSource.h"
#import "RMMarkerManager.h"
#import "RMMarker.h"
#import "MessageViewController.h"
#import "AppDelegate.h"

#import "ExtProtocol.pb.h"

@implementation ViewController

@synthesize currentSubscriberSSI;

@synthesize myMapView;
@synthesize myMenuButton;
@synthesize mySegmentMenu;
@synthesize myMessagesContainer;
@synthesize mySlideNotificationsButton;
@synthesize mySlideSubscribersButton;
@synthesize gps;
@synthesize myCenterMapButton;
@synthesize myTableCell;
@synthesize mySubscribersTable;
@synthesize myNotificationsTable;
@synthesize messagesController;
@synthesize popoverController;
@synthesize notifiController;
@synthesize historyController;
@synthesize myNavigationItem;
@synthesize connectionStateView;



#pragma mark - View life cicle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*TODO нормальное получение собственного SSI
     !!!!!!!!!!!!!!
     !!!!!!!!!!!!!*/
    self.currentSubscriberSSI=[NSNumber numberWithLong:105];
    /*
     !!!!!!!!!!!!!!
     !!!!!!!!!!!!!*/
    
    
    ExtProtocol::GeoMessage *pac=new ExtProtocol::GeoMessage;
    pac->set_ssi(111);
    pac->set_latitude(77);
    pac->set_longitude(55);
    NSData *data=[self getDataForGeoMessage:pac];
    ExtProtocol::GeoMessage *pac2=new ExtProtocol::GeoMessage;
    pac2=[self getGeoMessageFromNSData:data];
    
    NSLog(@"%f, - - %f",pac2->latitude(),
          pac2->longitude());
    
    //Инициализируем TCP соединение
    //TETRA
    [self TcpClientInitialise];
    //ACAPI
    [self ConnectTo:@"10.255.253.140" withPort:@"6766"];
    
    [self InitSubscribers];
    [self performSelector:@selector(subscribersTableReload)
               withObject:nil
               afterDelay:3];
    
    
    ///GIU configuration
    //Прячем контейнер с диалогом
    self.myMessagesContainer.hidden=YES;
    self.myMessagesContainer.alpha=0.0f;
    self.myCenterMapButton.hidden=NO;

    //SVGKit иконки для кнопок
    SVGKImage *showNotifiImg=[SVGKImage imageNamed:@"show-hide2.svg"];
    SVGKImage *showSubscrImg=[SVGKImage imageNamed:@"show-hide1.svg"];
    SVGKImage *centerImg=[SVGKImage imageNamed:@"location.svg"];
    
    SVGKImageView *notifiImageView=[[SVGKFastImageView alloc]initWithSVGKImage:showNotifiImg];
    [notifiImageView setFrame:CGRectMake(0, 0, mySlideNotificationsButton.frame.size.width,mySlideNotificationsButton.frame.size.height)];
    SVGKImageView *subscrImageView=[[SVGKFastImageView alloc]initWithSVGKImage:showSubscrImg];
    [subscrImageView setFrame:CGRectMake(0, 0, mySlideSubscribersButton.frame.size.width,mySlideSubscribersButton.frame.size.height)];
    SVGKImageView *centerImageView=[[SVGKFastImageView alloc]initWithSVGKImage:centerImg];
    [centerImageView setFrame:CGRectMake(0, 0, myCenterMapButton.frame.size.width,myCenterMapButton.frame.size.height)];
    //SVGKImageView закрывает кнопки, поэтому создаем нового обработчика событий касания по SVGKImageView
    UITapGestureRecognizer *subGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    UITapGestureRecognizer *notGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    UITapGestureRecognizer *cenGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [subscrImageView addGestureRecognizer:subGestureRecognizer];
    [centerImageView addGestureRecognizer:cenGestureRecognizer];
    [notifiImageView addGestureRecognizer:notGestureRecognizer];
    //Размещаем новые иконки
    [self.mySlideSubscribersButton addSubview:subscrImageView];
    [self.mySlideNotificationsButton addSubview:notifiImageView];
    [self.myCenterMapButton addSubview:centerImageView];
    
    //Инициализируем карту
    [RMMapView class];
    //Назначаем слушателя на события карты
    [myMapView setDelegate:self];
    
    //Слушатель обновления координат устройства
    self.gps = [CLLocationManager new];
    gps.delegate = self;
    gps.desiredAccuracy = kCLLocationAccuracyBest;
    [gps startUpdatingLocation];
    
    //Конфигурирование PopoverController для отображения вспывающего меню
    menuContent=[[PopoverContentViewController alloc]initWithNibName:nil bundle:nil];
    menuContent.viewDelegate=self;
    self.popoverController=[[UIPopoverController alloc]initWithContentViewController:menuContent];
    menuContent.popoverController=self.popoverController;
    [self.mySegmentMenu setSelectedSegmentIndex:0];
    clikIndex=nil;
    //Задаем размер контента окна меню
    [popoverController setPopoverContentSize:CGSizeMake(280.0f, 278.0f)];
    
    //Флаги для отображения состояния боковых меню
    isOpenSlideSubscribers=NO;
    isOpenSlydeNotifications=NO;
    //Флаги состояния серверов
    isOnlineServer=NO;
    //Конфигурация окна, отображающего соединение с серверами
    connectionStateView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 40)];
    connectionStateView.backgroundColor=[UIColor whiteColor];
    //Индикатор геоданных и сообщений
    UILabel *isolonaServerStateLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 10, 180, 20)];
    isolonaServerStateLabel.backgroundColor=[UIColor whiteColor];
    isolonaServerStateLabel.text=@"Isolona Server";
    [connectionStateView addSubview:isolonaServerStateLabel];
    SVGKImage *img=[SVGKImage imageNamed:@"indicator3.svg"];
    SVGKImageView *isolonaServerStateImage=[[SVGKFastImageView alloc] initWithSVGKImage:img];
    [isolonaServerStateImage setFrame:CGRectMake(0, 10, 20, 20)];
    [connectionStateView addSubview:isolonaServerStateImage];
    [self.myNavigationItem setTitleView:connectionStateView];
    
    //Делегаты
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    self.messagesController=delegate.myMessagesDelegate;
    notifiController=[[DispetcherNotificationController alloc] initWithTableView:myNotificationsTable];
    messagesController.ViewControllerDelegat=self;
    //messagesController.notifiControllerDelegat=self.notifiController;
    delegate.myViewControllerDelegate=self;
    delegate.myNavigationItem=self.myNavigationItem;
    
    //Конфигурация таблицы с абонентами
    self.mySubscribersTable.dataSource=self;
    self.mySubscribersTable.delegate=self;
    self.mySubscribersTable.layer.borderWidth=1.0;
    self.mySubscribersTable.layer.borderColor=[[UIColor grayColor]CGColor];
    self.mySubscribersTable.alwaysBounceVertical=NO;

    
    //Конфигурация контроллера уведомлений и таблицы для уведомлений диспетчера.
    self.myNotificationsTable.dataSource=notifiController;
    self.myNotificationsTable.delegate=notifiController;
    self.myNotificationsTable.layer.borderWidth=1.0;
    self.myNotificationsTable.layer.borderColor=[[UIColor grayColor]CGColor];
    self.myNotificationsTable.alwaysBounceVertical=NO;

    
    //Локализация строк
    self.myMenuButton.title=NSLocalizedString(@"MENUBUTTON", nil);
    [self.mySegmentMenu  setTitle:NSLocalizedString(@"MAP", nil) forSegmentAtIndex:0];
    [self.mySegmentMenu  setTitle:NSLocalizedString(@"DIALOG", nil) forSegmentAtIndex:1];
    [self.mySegmentMenu  setTitle:NSLocalizedString(@"HISTORY", nil) forSegmentAtIndex:2];
    
    
    

    //Таймер для проверки соелинения с сервером
    reachabilityTimer=[NSTimer scheduledTimerWithTimeInterval:30
                                                 target:self
                                               selector:@selector(updateConnectionsStatus)
                                               userInfo:nil
                                                repeats:YES];
    [self performSelector:@selector(updateConnectionsStatus)
               withObject:nil
               afterDelay:2.0f];
    
    [self sendAtCTSDS];
}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [self.navigationItem setTitleView:delegate.myNavigationItem.titleView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Socket and parser methods
- (void)ConnectTo:(NSString*)ip
         withPort:(NSString*)port
{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;

    int nPort=[port intValue];
    
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)ip, nPort, &readStream, &writeStream);
    InputServerStream = (__bridge NSInputStream *)readStream;
    OutputServerStream = (__bridge NSOutputStream *)writeStream;
	
	[InputServerStream setDelegate:self];
	[OutputServerStream setDelegate:self];
	
	[InputServerStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[OutputServerStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	
	[InputServerStream open];
	[OutputServerStream open];
}

-(void)SendMessToXMLServer
{
    UInt8 mes=0x26;
    NSData *oneData = [[NSData alloc] initWithBytes:&mes length:sizeof(mes)];
    [OutputServerStream write:(const uint8_t*)[oneData bytes] maxLength:[oneData length]];
}



//Инициализация соединения
- (void)TcpClientInitialise
{
	NSLog(@"Tcp Client Initialise");
	
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //NSString *text = @"10.255.253.174";
    NSString *text = @"10.255.253.140";
    NSString *chislo=@"4001";
    //NSString *text = [defaults objectForKey:@"ip"];
    //NSString *chislo=[defaults objectForKey:@"port"];
    int port=[chislo intValue];
    
    CFStreamCreatePairWithSocketToHost(NULL, (__bridge CFStringRef)text, port, &readStream, &writeStream);
    InputStream = (__bridge NSInputStream *)readStream;
    OutputStream = (__bridge NSOutputStream *)writeStream;
	
	[InputStream setDelegate:self];
	[OutputStream setDelegate:self];
	
	[InputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[OutputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	
	[InputStream open];
	[OutputStream open];
}

-(void)sendAtCTSDS
{
    NSString *msg_ctsds=@"AT+CTSDS=12,0,0,0,0,0\r\n";
    NSData *oneData = [[NSData alloc] initWithData:[msg_ctsds dataUsingEncoding:NSASCIIStringEncoding]];
    [OutputStream write:(const uint8_t*)[oneData bytes] maxLength:[oneData length]];
}

- (void)sendSDSMessage:(NSString*)sds
                 toSSI:(NSNumber*)targetSSI
{
    if([OutputStream streamStatus]!=NSStreamStatusError)
    {
        NSMutableString *msg_cmgs=[[NSMutableString alloc]initWithString:@"AT+CMGS="];
        NSLog(@"Send message - %@",sds);
        NSString *comma=@",";
        NSString *lengthMes=[NSString stringWithFormat:@"%u",[sds length]*8+32];
        NSString *cr_lf=@"\r\n";
        NSString *serviceSymbol=@"8204FF05";
        unsigned char symbol=0x1a;
        [msg_cmgs appendString:[targetSSI stringValue]];
        [msg_cmgs appendString:comma];
        [msg_cmgs appendString:lengthMes];
        [msg_cmgs appendString:cr_lf];
        [msg_cmgs appendString:serviceSymbol];
        
        NSMutableString *text=[[NSMutableString alloc]initWithString:sds];
        NSMutableString *fullText=[[NSMutableString alloc]init];
        for(int i=0;i<[text length];i++)
        {
            NSString *bukva=[NSString stringWithFormat:@"%x",[text characterAtIndex:i]];
            if([bukva isEqualToString:@"410"])
            {[fullText appendString:@"B0"];}
            else if([bukva isEqualToString:@"411"])
            {[fullText appendString:@"B1"];}
            else if([bukva isEqualToString:@"412"])
            {[fullText appendString:@"B2"];}
            else if([bukva isEqualToString:@"413"])
            {[fullText appendString:@"B3"];}
            else if([bukva isEqualToString:@"414"])
            {[fullText appendString:@"B4"];}
            else if([bukva isEqualToString:@"415"])
            {[fullText appendString:@"B5"];}
            else if([bukva isEqualToString:@"416"])
            {[fullText appendString:@"B6"];}
            else if([bukva isEqualToString:@"417"])
            {[fullText appendString:@"B7"];}
            else if([bukva isEqualToString:@"418"])
            {[fullText appendString:@"B8"];}
            else if([bukva isEqualToString:@"419"])
            {[fullText appendString:@"B9"];}
            else if([bukva isEqualToString:@"41a"])
            {[fullText appendString:@"BA"];}
            else if([bukva isEqualToString:@"41b"])
            {[fullText appendString:@"BB"];}
            else if([bukva isEqualToString:@"41c"])
            {[fullText appendString:@"BC"];}
            else if([bukva isEqualToString:@"41d"])
            {[fullText appendString:@"BD"];}
            else if([bukva isEqualToString:@"41e"])
            {[fullText appendString:@"BE"];}
            else if([bukva isEqualToString:@"41f"])
            {[fullText appendString:@"BF"];}
            else if([bukva isEqualToString:@"420"])
            {[fullText appendString:@"C0"];}
            else if([bukva isEqualToString:@"421"])
            {[fullText appendString:@"C1"];}
            else if([bukva isEqualToString:@"422"])
            {[fullText appendString:@"C2"];}
            else if([bukva isEqualToString:@"423"])
            {[fullText appendString:@"C3"];}
            else if([bukva isEqualToString:@"424"])
            {[fullText appendString:@"C4"];}
            else if([bukva isEqualToString:@"425"])
            {[fullText appendString:@"C5"];}
            else if([bukva isEqualToString:@"426"])
            {[fullText appendString:@"C6"];}
            else if([bukva isEqualToString:@"427"])
            {[fullText appendString:@"C7"];}
            else if([bukva isEqualToString:@"428"])
            {[fullText appendString:@"C8"];}
            else if([bukva isEqualToString:@"429"])
            {[fullText appendString:@"C9"];}
            else if([bukva isEqualToString:@"42a"])
            {[fullText appendString:@"CA"];}
            else if([bukva isEqualToString:@"42b"])
            {[fullText appendString:@"CB"];}
            else if([bukva isEqualToString:@"42c"])
            {[fullText appendString:@"CC"];}
            else if([bukva isEqualToString:@"42d"])
            {[fullText appendString:@"CD"];}
            else if([bukva isEqualToString:@"42e"])
            {[fullText appendString:@"CE"];}
            else if([bukva isEqualToString:@"42f"])
            {[fullText appendString:@"CF"];}
            
            else if([bukva isEqualToString:@"430"])
            {[fullText appendString:@"D0"];}
            else if([bukva isEqualToString:@"431"])
            {[fullText appendString:@"D1"];}
            else if([bukva isEqualToString:@"432"])
            {[fullText appendString:@"D2"];}
            else if([bukva isEqualToString:@"433"])
            {[fullText appendString:@"D3"];}
            else if([bukva isEqualToString:@"434"])
            {[fullText appendString:@"D4"];}
            else if([bukva isEqualToString:@"435"])
            {[fullText appendString:@"D5"];}
            else if([bukva isEqualToString:@"436"])
            {[fullText appendString:@"D6"];}
            else if([bukva isEqualToString:@"437"])
            {[fullText appendString:@"D7"];}
            else if([bukva isEqualToString:@"438"])
            {[fullText appendString:@"D8"];}
            else if([bukva isEqualToString:@"439"])
            {[fullText appendString:@"D9"];}
            else if([bukva isEqualToString:@"43a"])
            {[fullText appendString:@"DA"];}
            else if([bukva isEqualToString:@"43b"])
            {[fullText appendString:@"DB"];}
            else if([bukva isEqualToString:@"43c"])
            {[fullText appendString:@"DC"];}
            else if([bukva isEqualToString:@"43d"])
            {[fullText appendString:@"DD"];}
            else if([bukva isEqualToString:@"43e"])
            {[fullText appendString:@"DE"];}
            else if([bukva isEqualToString:@"43f"])
            {[fullText appendString:@"DF"];}
            
            else if([bukva isEqualToString:@"440"])
            {[fullText appendString:@"E0"];}
            else if([bukva isEqualToString:@"441"])
            {[fullText appendString:@"E1"];}
            else if([bukva isEqualToString:@"442"])
            {[fullText appendString:@"E2"];}
            else if([bukva isEqualToString:@"443"])
            {[fullText appendString:@"E3"];}
            else if([bukva isEqualToString:@"444"])
            {[fullText appendString:@"E4"];}
            else if([bukva isEqualToString:@"445"])
            {[fullText appendString:@"E5"];}
            else if([bukva isEqualToString:@"446"])
            {[fullText appendString:@"E6"];}
            else if([bukva isEqualToString:@"447"])
            {[fullText appendString:@"E7"];}
            else if([bukva isEqualToString:@"448"])
            {[fullText appendString:@"E8"];}
            else if([bukva isEqualToString:@"449"])
            {[fullText appendString:@"E9"];}
            else if([bukva isEqualToString:@"44a"])
            {[fullText appendString:@"EA"];}
            else if([bukva isEqualToString:@"44b"])
            {[fullText appendString:@"EB"];}
            else if([bukva isEqualToString:@"44c"])
            {[fullText appendString:@"EC"];}
            else if([bukva isEqualToString:@"44d"])
            {[fullText appendString:@"ED"];}
            else if([bukva isEqualToString:@"44e"])
            {[fullText appendString:@"EE"];}
            else if([bukva isEqualToString:@"44f"])
            {[fullText appendString:@"EF"];}
            else{[fullText appendString:bukva];}
        }
        
        NSData *threeData =[[NSData alloc] initWithData:[fullText dataUsingEncoding:NSASCIIStringEncoding]];
        NSMutableData *twoData = [[NSMutableData alloc] initWithData:[msg_cmgs dataUsingEncoding:NSASCIIStringEncoding]];
        [twoData appendData:threeData];
        [twoData appendBytes:&symbol length:1];
        
        [OutputStream write:(const uint8_t*)[twoData bytes] maxLength:[twoData length]];
        }
    else
    {
        //если поток не открыт
        NSLog(@"TCP Client - Can't connect to the host (OUTPUT STREAM)");
    }
}

- (NSData *)dataFromHexString:(NSString*)string {
    const char *chars = [string UTF8String];
    int i = 0, len = string.length;
    
    NSMutableData *data = [NSMutableData dataWithCapacity:len / 2];
    char byteChars[3] = {'\0','\0','\0'};
    unsigned long wholeByte;
    
    while (i < len) {
        byteChars[0] = chars[i++];
        byteChars[1] = chars[i++];
        wholeByte = strtoul(byteChars, NULL, 16);
        [data appendBytes:&wholeByte length:1];
    }
    
    return data;
}

-(void)geoDataParser:(NSString*)dataString
{
    //получаем данные из hex строки
    NSData *aData=[self dataFromHexString:dataString];
    
    ExtProtocol::GeoMessage *packet = new ExtProtocol::GeoMessage;
    packet=[self getGeoMessageFromNSData:aData];
    NSLog(@"%f, - - %f",packet->latitude(),packet->longitude());
    
    uint32_t X=packet->longitude();
    double grad1=360;
    double dva1=1;
    int mnog1=2;
    for(int i=0;i<25;i++)
        dva1=dva1*mnog1;
    double x=X/dva1*grad1;
    NSLog(@"Longitude - %f",x);
    
    
    uint32_t Y=packet->latitude();
    double grad=180;
    double dva=1;
    int mnog=2;
    for(int i=0;i<24;i++)
        dva=dva*mnog;
    double y=Y/dva*grad;
    NSLog(@"Latitude - %f",y);

    NSString *latitudeValue=[NSString stringWithFormat:@"%f",y];
    NSString *longitudeValue=[NSString stringWithFormat:@"%f",x];
    NSNumber *senderSSI=[NSNumber numberWithLong:packet->ssi()];
    
    //передвигаем маркер на карте
    [markerManager moveMarker:[markersDictionary valueForKey:[senderSSI stringValue]]
                     AtLatLon:CLLocationCoordinate2DMake( [latitudeValue doubleValue],[longitudeValue doubleValue])];
    
    //сохранняем в базу
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Subscribers"];
    NSArray *subscrbs=[context executeFetchRequest:fetchRequest error:nil];
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * longtitudeNumber = [f numberFromString:longitudeValue];
    NSNumber * latitudeNumber = [f numberFromString:latitudeValue];
    for(NSManagedObject *obj in subscrbs)
    {
        if([[obj valueForKey:@"ssi"] longValue]==[senderSSI longValue])
        {
            [obj setValue:longtitudeNumber forKey:@"longitude"];
            [obj setValue:latitudeNumber forKey:@"latitude"];
            break;
        }
    }
    NSError *error = nil;
    if(![context save:&error]){
        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
    }
}

-(NSMutableString*)simpleDataParser:(NSString*)rawDataString
{
    NSMutableString* inputMessage=[[NSMutableString alloc] initWithString:@""];
    
    for(int i=0;i<[rawDataString length]-1;i=i+2)
    {
        NSRange change=NSMakeRange(i, 2);
        NSString *bukva=[rawDataString substringWithRange:change];
        //NSLog(@"Bukva - %@\n",bukva);
        //English ISO 8859-5
        if([bukva isEqualToString:@"20"])
        {[inputMessage appendString:@" "];}
        else if([bukva isEqualToString:@"21"])
        {[inputMessage appendString:@"!"];}
        else if([bukva isEqualToString:@"22"])
        {[inputMessage appendString:@"''"];}
        else if([bukva isEqualToString:@"23"])
        {[inputMessage appendString:@"#"];}
        else if([bukva isEqualToString:@"24"])
        {[inputMessage appendString:@"$"];}
        else if([bukva isEqualToString:@"25"])
        {[inputMessage appendString:@"%"];}
        else if([bukva isEqualToString:@"26"])
        {[inputMessage appendString:@"&"];}
        else if([bukva isEqualToString:@"27"])
        {[inputMessage appendString:@"'"];}
        else if([bukva isEqualToString:@"28"])
        {[inputMessage appendString:@"("];}
        else if([bukva isEqualToString:@"29"])
        {[inputMessage appendString:@")"];}
        else if([bukva isEqualToString:@"2A"])
        {[inputMessage appendString:@"*"];}
        else if([bukva isEqualToString:@"2B"])
        {[inputMessage appendString:@"+"];}
        else if([bukva isEqualToString:@"2C"])
        {[inputMessage appendString:@","];}
        else if([bukva isEqualToString:@"2D"])
        {[inputMessage appendString:@"-"];}
        else if([bukva isEqualToString:@"2E"])
        {[inputMessage appendString:@"."];}
        else if([bukva isEqualToString:@"2F"])
        {[inputMessage appendString:@"/"];}
        else if([bukva isEqualToString:@"30"])
        {[inputMessage appendString:@"0"];}
        else if([bukva isEqualToString:@"31"])
        {[inputMessage appendString:@"1"];}
        else if([bukva isEqualToString:@"32"])
        {[inputMessage appendString:@"2"];}
        else if([bukva isEqualToString:@"33"])
        {[inputMessage appendString:@"3"];}
        else if([bukva isEqualToString:@"34"])
        {[inputMessage appendString:@"4"];}
        else if([bukva isEqualToString:@"35"])
        {[inputMessage appendString:@"5"];}
        else if([bukva isEqualToString:@"36"])
        {[inputMessage appendString:@"6"];}
        else if([bukva isEqualToString:@"37"])
        {[inputMessage appendString:@"7"];}
        else if([bukva isEqualToString:@"38"])
        {[inputMessage appendString:@"8"];}
        else if([bukva isEqualToString:@"39"])
        {[inputMessage appendString:@"9"];}
        else if([bukva isEqualToString:@"3A"])
        {[inputMessage appendString:@":"];}
        else if([bukva isEqualToString:@"3B"])
        {[inputMessage appendString:@";"];}
        else if([bukva isEqualToString:@"3C"])
        {[inputMessage appendString:@"<"];}
        else if([bukva isEqualToString:@"3D"])
        {[inputMessage appendString:@"="];}
        else if([bukva isEqualToString:@"3E"])
        {[inputMessage appendString:@">"];}
        else if([bukva isEqualToString:@"3F"])
        {[inputMessage appendString:@"?"];}
        else if([bukva isEqualToString:@"40"])
        {[inputMessage appendString:@"@"];}
        else if([bukva isEqualToString:@"41"])
        {[inputMessage appendString:@"A"];}
        else if([bukva isEqualToString:@"42"])
        {[inputMessage appendString:@"B"];}
        else if([bukva isEqualToString:@"43"])
        {[inputMessage appendString:@"C"];}
        else if([bukva isEqualToString:@"44"])
        {[inputMessage appendString:@"D"];}
        else if([bukva isEqualToString:@"45"])
        {[inputMessage appendString:@"E"];}
        else if([bukva isEqualToString:@"46"])
        {[inputMessage appendString:@"F"];}
        else if([bukva isEqualToString:@"47"])
        {[inputMessage appendString:@"G"];}
        else if([bukva isEqualToString:@"48"])
        {[inputMessage appendString:@"(H"];}
        else if([bukva isEqualToString:@"49"])
        {[inputMessage appendString:@"I"];}
        else if([bukva isEqualToString:@"4A"])
        {[inputMessage appendString:@"J"];}
        else if([bukva isEqualToString:@"4B"])
        {[inputMessage appendString:@"K"];}
        else if([bukva isEqualToString:@"4C"])
        {[inputMessage appendString:@"L"];}
        else if([bukva isEqualToString:@"4D"])
        {[inputMessage appendString:@"M"];}
        else if([bukva isEqualToString:@"4E"])
        {[inputMessage appendString:@"N"];}
        else if([bukva isEqualToString:@"4F"])
        {[inputMessage appendString:@"O"];}
        else if([bukva isEqualToString:@"50"])
        {[inputMessage appendString:@"P"];}
        else if([bukva isEqualToString:@"51"])
        {[inputMessage appendString:@"Q"];}
        else if([bukva isEqualToString:@"52"])
        {[inputMessage appendString:@"R"];}
        else if([bukva isEqualToString:@"53"])
        {[inputMessage appendString:@"S"];}
        else if([bukva isEqualToString:@"54"])
        {[inputMessage appendString:@"T"];}
        else if([bukva isEqualToString:@"55"])
        {[inputMessage appendString:@"U"];}
        else if([bukva isEqualToString:@"56"])
        {[inputMessage appendString:@"V"];}
        else if([bukva isEqualToString:@"57"])
        {[inputMessage appendString:@"W"];}
        else if([bukva isEqualToString:@"58"])
        {[inputMessage appendString:@"X"];}
        else if([bukva isEqualToString:@"59"])
        {[inputMessage appendString:@"Y"];}
        else if([bukva isEqualToString:@"5A"])
        {[inputMessage appendString:@"Z"];}
        else if([bukva isEqualToString:@"5B"])
        {[inputMessage appendString:@"["];}
        else if([bukva isEqualToString:@"5C"])
        {[inputMessage appendString:@""];}
        else if([bukva isEqualToString:@"5D"])
        {[inputMessage appendString:@"]"];}
        else if([bukva isEqualToString:@"5E"])
        {[inputMessage appendString:@"^"];}
        else if([bukva isEqualToString:@"5F"])
        {[inputMessage appendString:@"_"];}
        else if([bukva isEqualToString:@"60"])
        {[inputMessage appendString:@"`"];}
        else if([bukva isEqualToString:@"61"])
        {[inputMessage appendString:@"a"];}
        else if([bukva isEqualToString:@"62"])
        {[inputMessage appendString:@"b"];}
        else if([bukva isEqualToString:@"63"])
        {[inputMessage appendString:@"c"];}
        else if([bukva isEqualToString:@"64"])
        {[inputMessage appendString:@"d"];}
        else if([bukva isEqualToString:@"65"])
        {[inputMessage appendString:@"e"];}
        else if([bukva isEqualToString:@"66"])
        {[inputMessage appendString:@"f"];}
        else if([bukva isEqualToString:@"67"])
        {[inputMessage appendString:@"g"];}
        else if([bukva isEqualToString:@"68"])
        {[inputMessage appendString:@"h"];}
        else if([bukva isEqualToString:@"69"])
        {[inputMessage appendString:@"i"];}
        else if([bukva isEqualToString:@"6A"])
        {[inputMessage appendString:@"j"];}
        else if([bukva isEqualToString:@"6B"])
        {[inputMessage appendString:@"k"];}
        else if([bukva isEqualToString:@"6C"])
        {[inputMessage appendString:@"l"];}
        else if([bukva isEqualToString:@"6D"])
        {[inputMessage appendString:@"m"];}
        else if([bukva isEqualToString:@"6E"])
        {[inputMessage appendString:@"n"];}
        else if([bukva isEqualToString:@"6F"])
        {[inputMessage appendString:@"o"];}
        else if([bukva isEqualToString:@"70"])
        {[inputMessage appendString:@"p"];}
        else if([bukva isEqualToString:@"71"])
        {[inputMessage appendString:@"q"];}
        else if([bukva isEqualToString:@"72"])
        {[inputMessage appendString:@"r"];}
        else if([bukva isEqualToString:@"73"])
        {[inputMessage appendString:@"s"];}
        else if([bukva isEqualToString:@"74"])
        {[inputMessage appendString:@"t"];}
        else if([bukva isEqualToString:@"75"])
        {[inputMessage appendString:@"u"];}
        else if([bukva isEqualToString:@"76"])
        {[inputMessage appendString:@"v"];}
        else if([bukva isEqualToString:@"77"])
        {[inputMessage appendString:@"w"];}
        else if([bukva isEqualToString:@"78"])
        {[inputMessage appendString:@"x"];}
        else if([bukva isEqualToString:@"79"])
        {[inputMessage appendString:@"y"];}
        else if([bukva isEqualToString:@"7A"])
        {[inputMessage appendString:@"z"];}
        else if([bukva isEqualToString:@"7B"])
        {[inputMessage appendString:@"{"];}
        else if([bukva isEqualToString:@"7C"])
        {[inputMessage appendString:@"|"];}
        else if([bukva isEqualToString:@"7D"])
        {[inputMessage appendString:@"}"];}
        else if([bukva isEqualToString:@"7E"])
        {[inputMessage appendString:@"~"];}
        else if([bukva isEqualToString:@"7F"])
        {[inputMessage appendString:@""];}
        //Russian ISO 8859-5
        else  if([bukva isEqualToString:@"B0"])
        {[inputMessage appendString:@"А"];}
        else if([bukva isEqualToString:@"B1"])
        {[inputMessage appendString:@"Б"];}
        else if([bukva isEqualToString:@"B2"])
        {[inputMessage appendString:@"В"];}
        else if([bukva isEqualToString:@"B3"])
        {[inputMessage appendString:@"Г"];}
        else if([bukva isEqualToString:@"B4"])
        {[inputMessage appendString:@"Д"];}
        else if([bukva isEqualToString:@"B5"])
        {[inputMessage appendString:@"Е"];}
        else if([bukva isEqualToString:@"B6"])
        {[inputMessage appendString:@"Ж"];}
        else if([bukva isEqualToString:@"B7"])
        {[inputMessage appendString:@"З"];}
        else if([bukva isEqualToString:@"B8"])
        {[inputMessage appendString:@"И"];}
        else if([bukva isEqualToString:@"B9"])
        {[inputMessage appendString:@"Й"];}
        else if([bukva isEqualToString:@"BA"])
        {[inputMessage appendString:@"К"];}
        else if([bukva isEqualToString:@"BB"])
        {[inputMessage appendString:@"Л"];}
        else if([bukva isEqualToString:@"BC"])
        {[inputMessage appendString:@"М"];}
        else if([bukva isEqualToString:@"BD"])
        {[inputMessage appendString:@"Н"];}
        else if([bukva isEqualToString:@"BE"])
        {[inputMessage appendString:@"О"];}
        else if([bukva isEqualToString:@"BF"])
        {[inputMessage appendString:@"П"];}
        else if([bukva isEqualToString:@"C0"])
        {[inputMessage appendString:@"Р"];}
        else if([bukva isEqualToString:@"C1"])
        {[inputMessage appendString:@"С"];}
        else if([bukva isEqualToString:@"C2"])
        {[inputMessage appendString:@"Т"];}
        else if([bukva isEqualToString:@"C3"])
        {[inputMessage appendString:@"У"];}
        else if([bukva isEqualToString:@"C4"])
        {[inputMessage appendString:@"Ф"];}
        else if([bukva isEqualToString:@"C5"])
        {[inputMessage appendString:@"Х"];}
        else if([bukva isEqualToString:@"C6"])
        {[inputMessage appendString:@"Ц"];}
        else if([bukva isEqualToString:@"C7"])
        {[inputMessage appendString:@"Ч"];}
        else if([bukva isEqualToString:@"C8"])
        {[inputMessage appendString:@"Ш"];}
        else if([bukva isEqualToString:@"C9"])
        {[inputMessage appendString:@"Щ"];}
        else if([bukva isEqualToString:@"CA"])
        {[inputMessage appendString:@"Ъ"];}
        else if([bukva isEqualToString:@"CB"])
        {[inputMessage appendString:@"Ы"];}
        else if([bukva isEqualToString:@"CC"])
        {[inputMessage appendString:@"Ь"];}
        else if([bukva isEqualToString:@"CD"])
        {[inputMessage appendString:@"Э"];}
        else if([bukva isEqualToString:@"CE"])
        {[inputMessage appendString:@"Ю"];}
        else if([bukva isEqualToString:@"CF"])
        {[inputMessage appendString:@"Я"];}
        
        else if([bukva isEqualToString:@"D0"])
        {[inputMessage appendString:@"а"];}
        else if([bukva isEqualToString:@"D1"])
        {[inputMessage appendString:@"б"];}
        else if([bukva isEqualToString:@"D2"])
        {[inputMessage appendString:@"в"];}
        else if([bukva isEqualToString:@"D3"])
        {[inputMessage appendString:@"г"];}
        else if([bukva isEqualToString:@"D4"])
        {[inputMessage appendString:@"д"];}
        else if([bukva isEqualToString:@"D5"])
        {[inputMessage appendString:@"е"];}
        else if([bukva isEqualToString:@"D6"])
        {[inputMessage appendString:@"ж"];}
        else if([bukva isEqualToString:@"D7"])
        {[inputMessage appendString:@"з"];}
        else if([bukva isEqualToString:@"D8"])
        {[inputMessage appendString:@"и"];}
        else if([bukva isEqualToString:@"D9"])
        {[inputMessage appendString:@"й"];}
        else if([bukva isEqualToString:@"DA"])
        {[inputMessage appendString:@"к"];}
        else if([bukva isEqualToString:@"DB"])
        {[inputMessage appendString:@"л"];}
        else if([bukva isEqualToString:@"DC"])
        {[inputMessage appendString:@"м"];}
        else if([bukva isEqualToString:@"DD"])
        {[inputMessage appendString:@"н"];}
        else if([bukva isEqualToString:@"DE"])
        {[inputMessage appendString:@"о"];}
        else if([bukva isEqualToString:@"DF"])
        {[inputMessage appendString:@"п"];}
        else if([bukva isEqualToString:@"E0"])
        {[inputMessage appendString:@"р"];}
        else if([bukva isEqualToString:@"E1"])
        {[inputMessage appendString:@"с"];}
        else if([bukva isEqualToString:@"E2"])
        {[inputMessage appendString:@"т"];}
        else if([bukva isEqualToString:@"E3"])
        {[inputMessage appendString:@"у"];}
        else if([bukva isEqualToString:@"E4"])
        {[inputMessage appendString:@"ф"];}
        else if([bukva isEqualToString:@"E5"])
        {[inputMessage appendString:@"х"];}
        else if([bukva isEqualToString:@"E6"])
        {[inputMessage appendString:@"ц"];}
        else if([bukva isEqualToString:@"E7"])
        {[inputMessage appendString:@"ч"];}
        else if([bukva isEqualToString:@"E8"])
        {[inputMessage appendString:@"ш"];}
        else if([bukva isEqualToString:@"E9"])
        {[inputMessage appendString:@"щ"];}
        else if([bukva isEqualToString:@"EA"])
        {[inputMessage appendString:@"ъ"];}
        else if([bukva isEqualToString:@"EB"])
        {[inputMessage appendString:@"ы"];}
        else if([bukva isEqualToString:@"EC"])
        {[inputMessage appendString:@"ь"];}
        else if([bukva isEqualToString:@"ED"])
        {[inputMessage appendString:@"э"];}
        else if([bukva isEqualToString:@"EE"])
        {[inputMessage appendString:@"ю"];}
        else if([bukva isEqualToString:@"EF"])
        {[inputMessage appendString:@"я"];}
        //else{[inputMessage appendString:bukva];}
    }
    return inputMessage;
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)StreamEvent
{
    switch (StreamEvent)
	{
		case NSStreamEventOpenCompleted:
        {
            if (theStream == OutputStream)
			{
                NSLog(@"TCP Client - OutputStream opened");
            }
            else if(theStream == InputStream)
            {
                NSLog(@"TCP Client - InputStream opened");
            }
            else if (theStream == InputServerStream)
            {
                NSLog(@"TCP Client - InputServerStream opened");
                [self performSelector:@selector(SendMessToXMLServer)
                           withObject:nil
                           afterDelay:1];
            }
		}
            break;
			
		case NSStreamEventHasBytesAvailable:
			if (theStream == InputStream)
			{
				uint8_t buffer[1024];
				int len;
				sleep(1);
				while ([InputStream hasBytesAvailable])
				{
					len = [InputStream read:buffer maxLength:sizeof(buffer)];
					if (len > 0)
					{
                        NSString *output=[[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        NSLog(@"RAW DATA STRING - %@", output);
                        
                        //поиск строковых констант в сообщении
                        NSRange findCTSDSR=[output rangeOfString:@"+CTSDSR"];
                        NSRange findSdsHeader=[output rangeOfString:@"82"];
                        NSRange findGeoHeader=[output rangeOfString:@"FE"];

                        //Разбиваем сообщение на части
                        NSMutableCharacterSet *workingSet=[[NSMutableCharacterSet alloc] init];
                        [workingSet addCharactersInString:@",\r\n"];
                        NSArray *outputStringPatrts=[output componentsSeparatedByCharactersInSet:workingSet];
                        
                        switch ([outputStringPatrts count]) {
                            case 12:
                            {
                                //Входящее
                                //SSI отправивший нам сообщение
                                NSString *subString=[outputStringPatrts objectAtIndex:3];
                                NSString *stringSSI=[subString substringFromIndex:12];
                                NSNumberFormatter* formatter=[[NSNumberFormatter alloc] init];
                                NSNumber *senderSSI=[formatter numberFromString:stringSSI];
                                
                                NSString *data=[[outputStringPatrts objectAtIndex:9] substringFromIndex:8];
                                NSString *parseResult=[self simpleDataParser:data];
                                
                                if (findSdsHeader.location!=NSNotFound) {
                                    [[self messagesController] incomingSDSMessage:parseResult
                                                                          fromSSI:senderSSI];
                                    
                                    ///Увеличиваем счетчик непрочитанных сообщений
                                    NSNumber *counter=(NSNumber*)[unreadMess valueForKey:[senderSSI stringValue]];
                                    int value=[counter integerValue];
                                    counter=[NSNumber numberWithInt:value+1];
                                    [unreadMess setValue:counter forKey:[senderSSI stringValue]];
                                }else if (findGeoHeader.location!=NSNotFound){
                                    [self geoDataParser:data];
                                }
                            }
                                break;
//                            case 26:
//                                //Исходящее сообщение дошло
//                                NSLog(@"Mes delivered");
//                                [[self messagesController] wasDelivered];
//                                break;
//                            case 38:
//                                //Исходящее сообщение не дошло
//                                NSLog(@"Mes was not delivered");
//                                break;
                            default:
                                NSLog(@"Unknown type inc mes");
                                break;
                        }
                        /*
                         //NSLog(@"Length of data- %lu\n",(unsigned long)[output length]);
                         NSRange findCTSDSR=[output rangeOfString:@"+CTSDSR"];
                         NSRange findCMGS=[output rangeOfString:@"+CMGS"];
                         NSRange status=[output rangeOfString:@"7E"];
                         if(findCTSDSR.location!=NSNotFound && status.location==NSNotFound)
                         {
                         NSLog(@"Входящее сообщение(+CTSDSR) %lu",(unsigned long)[output length]);
                         NSMutableString *inputMessage=[[NSMutableString alloc]initWithString:@""];
                         NSRange rangeGEO=[output rangeOfString:@"FE"];
                         NSRange rangeSDS=[output rangeOfString:@"82"];
                         NSString *newString;
                         if (((rangeGEO.location==NSNotFound)||(rangeGEO.location!=56))&&(rangeSDS.location!=NSNotFound))
                         {
                         newString=[output substringFromIndex:rangeSDS.location+8];
                         //Это обычное сообщение
                         //NSLog(@"NewString - %@\n",newString);
                         //Вычленяем из сообщения информацию об отправителе
                         NSArray *stringPatrts=[output componentsSeparatedByString:@","];
                         //SSI отправивший нам сообщение
                         NSString *subString=[stringPatrts objectAtIndex:1];
                         NSString *stringSSI=[subString substringFromIndex:12];
                         NSLog(@"Sender SSI - %@",stringSSI);
                         NSNumberFormatter* formatter=[[NSNumberFormatter alloc] init];
                         NSNumber *senderSSI=[formatter numberFromString:stringSSI];
                         
                         //Парсим сообщение
                         [inputMessage appendString:[self simpleDataParser:newString]];
                         
                         ///Увеличиваем счетчик непрочитанных сообщений
                         NSNumber *counter=(NSNumber*)[unreadMess valueForKey:[senderSSI stringValue]];
                         int value=[counter integerValue];
                         counter=[NSNumber numberWithInt:value+1];
                         [unreadMess setValue:counter forKey:[senderSSI stringValue]];
                         
                         for(SubscriberTableViewCell* cell in [mySubscribersTable visibleCells])
                         {
                         NSNumber *ssi=[formatter numberFromString:cell.ssiLabel.text];
                         [self checkUnreadMessForSSI:ssi];
                         }
                         //Уведомление о том, что пришло сообщение
                         NSLog(@"%@",inputMessage);
                         if (menuContent.playSounds) {
                         [SoundSDS playSound:@"example.caf"];
                         }
                         [self.messagesController incomingSDSMessage:inputMessage
                         fromSSI:senderSSI];
                         
                         //Метод для добавления уведомления в таблицу уведомлений справа
                         NSArray *obj=[[NSArray alloc]initWithObjects:[NSNumber numberWithInt:NotifiTypeTextMessage],
                         [senderSSI stringValue],
                         nil];
                         NSArray *key=[[NSArray alloc]initWithObjects:@"NotifiType",@"info", nil];
                         NSDictionary *dic=[[NSDictionary alloc]initWithObjects:obj forKeys:key];
                         [self.notifiController addNotificationWithDictionary:dic];
                         
                         
                         ///Обновление истории сообщений
                         [self performSelector:@selector(updateHistory)
                         withObject:nil
                         afterDelay:1.0];
                         }
                         else if((rangeGEO.location!=NSNotFound)&&(rangeGEO.location==56))
                         {
                         //Если заголовок соответствует НАШЕМУ заголовку геоданных
                         newString=[output substringFromIndex:rangeGEO.location+8];
                         [self geoDataParser:newString];
                         }
                         else
                         {
                         NSLog(@"Undifinded header type!");
                         }
                         }
                         else if (findCMGS.location!=NSNotFound)
                         {
                         NSString *str=@"Send message successul \u2713";
                         [messagesController wasDelivered];
                         }

                         */
                    }

                }
			}
            else if (theStream == InputServerStream)
            {
                NSLog(@"XMLserver");
                uint8_t buffer[1024];
				int len;
				sleep(1);
				while ([InputServerStream hasBytesAvailable])
				{
					len = [InputServerStream read:buffer maxLength:sizeof(buffer)];
					if (len > 0)
					{
                        NSString *ssiString=[[NSString alloc] initWithBytes:buffer length:len encoding:NSUTF8StringEncoding];
                        //NSLog(@"RAW DATA STRING - %@", ssiString);
                        ssiList = (NSMutableArray*)[ssiString componentsSeparatedByString:@"\n"];
                        [ssiList removeLastObject];
                        [self InitSubscribers];
                    }
                }

            }
			break;
			
		case NSStreamEventErrorOccurred:
        {
            if(theStream==InputServerStream)
            {
                NSLog(@"sabaka!");
            }
            NSLog(@"TCP Client - Can't connect to the host");
            //Метод для проверки добавления уведомления
            NSArray *obj=[[NSArray alloc]initWithObjects:[NSNumber numberWithInt:NotifiTypeSystemMessage],NSLocalizedString(@"OFFCONNECTION", nil), nil];
            NSArray *key=[[NSArray alloc]initWithObjects:@"NotifiType",@"info", nil];
            NSDictionary *dic=[[NSDictionary alloc]initWithObjects:obj forKeys:key];
            [self.notifiController addNotificationWithDictionary:dic];
            
        }
			break;
			
		case NSStreamEventEndEncountered:
            if(theStream==InputServerStream)
            {
                NSLog(@"sabaka!");
            }
			NSLog(@"TCP Client - End encountered");
			[theStream close];
			[theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
			break;
            
		case NSStreamEventNone:
			NSLog(@"TCP Client - None event");
			break;
			
		case NSStreamEventHasSpaceAvailable:
			NSLog(@"TCP Client - Has space available event");
			break;
            
		default:
			NSLog(@"TCP Client - Unknown event");
	}
	
}

#pragma mark Protobuf
- (ExtProtocol::GeoMessage *)getGeoMessageFromNSData:(NSData *)data {
    char raw[[data length]];
    ExtProtocol::GeoMessage *packet = new ExtProtocol::GeoMessage;
    [data getBytes:raw length:[data length]];
    packet->ParseFromArray(raw, [data length]);
    return packet;
}

- (NSData *)getDataForGeoMessage:(ExtProtocol::GeoMessage *)packet {
    std::string ps = packet->SerializeAsString();
    return [NSData dataWithBytes:ps.c_str() length:ps.size()];
}

#pragma mark Table view methods

-(void) subscribersTableReload
{
    [[self mySubscribersTable] reloadData];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return selectedSubscribers.count;
}

//Отрисовка таблицы
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    myTableCell=nil;
    //SVGKImage *myImage;
    UIImage *myImage;
    
    if([tableView isEqual:self.mySubscribersTable])
    {
        NSNumber *ssi=[[selectedSubscribers objectAtIndex:indexPath.row] valueForKey:@"ssi"];
        NSString *name=[[selectedSubscribers objectAtIndex:indexPath.row] valueForKey:@"name"];
        
        static NSString *CellTable=@"SubscriberCell";
        
        myTableCell=(SubscriberTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellTable];
        if(myTableCell==nil)
        {
            ///Загружаем нашу собственную ячейку (custom cell)
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SubscriberTableViewCell"
                                                         owner:self
                                                       options:nil];
            myTableCell = [nib objectAtIndex:0];
        }
        
        myTableCell.ssiLabel.text=[ssi stringValue];
        
        myTableCell.nameLabel.textAlignment=NSTextAlignmentLeft;
        myTableCell.nameLabel.backgroundColor=myTableCell.backgroundColor;
        myTableCell.nameLabel.text=name;

        /*
        myImage=[SVGKImage imageNamed:@"abonent4.svg"];
        SVGKImageView *mySvgImageView=[[SVGKLayeredImageView alloc]initWithFrame:myTableCell.subscrImage.frame];
        SVGKImageView *mySvgImageView=[[SVGKFastImageView alloc]initWithFrame:myTableCell.subscrImage.frame];
         [mySvgImageView setImage:myImage];
         [myTableCell addSubview:mySvgImageView];
         */
        myImage=[UIImage imageNamed:@"marker-red.png"];
        [myTableCell.subscrImage setImage:myImage];

        myTableCell.backgroundColor=[UIColor lightGrayColor];
        
        ///Уведомление о входящих сообщениях
        NSString* keySSI=[ssi stringValue];
        int counter=[[unreadMess valueForKey:keySSI] intValue];
        if (counter!=0)
        {
            NSString *unread=[NSString stringWithFormat:@"+%@", [[unreadMess valueForKey:keySSI] stringValue]];
            myTableCell.noticeLabel.text=unread;
        }
        else
        {
            myTableCell.noticeLabel.text=nil;
        }
    }
    
    return myTableCell;
}


///Клик по ячейке таблицы
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int segmentIndex=(int)mySegmentMenu.selectedSegmentIndex;
    myTableCell = (SubscriberTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    NSNumberFormatter* formatter=[[NSNumberFormatter alloc] init];
    NSNumber *ssi=[formatter numberFromString:myTableCell.ssiLabel.text];
    
    switch (segmentIndex) {
        //Выбран пункт карта
        case 0:
        {
            //Переменститья на карте к маркеру абонента выбранного в таблице
            [self localeToSubscriberWithSSI:ssi];
        }
        break;
         
        //Выбран пункт диалог
        case 1:
        {
            //Отобразить диалоговое окно с абонентом
            [self selectSubscriberWithSSI:ssi];
        }
        break;
    }
    clikIndex=indexPath;

}
#pragma mark - GUI methods

- (IBAction)segmentMenuSelected:(id)sender
{
    [popoverController dismissPopoverAnimated:YES];
    
    if(mySegmentMenu.selectedSegmentIndex==0)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [self hideDialog];
        
        mySlideNotificationsButton.hidden=NO;
        mySlideSubscribersButton.hidden=NO;
        if (isOpenSlideSubscribers)
            [self showSubscribersTable];
        else
            [self hideSubscribersTable];
        
        if (isOpenSlydeNotifications)
            [self showNotificationsTable];
        else
            [self hideNotificationsTable];
    }
    if(mySegmentMenu.selectedSegmentIndex==1)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];

        if (clikIndex==nil)
        {
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
            clikIndex=indexPath;
        }
        
        [self tableView:mySubscribersTable didSelectRowAtIndexPath:clikIndex];
        
        
        [self showSubscribersTable];
        [self hideNotificationsTable];
       
        mySlideNotificationsButton.hidden=YES;
        mySlideSubscribersButton.hidden=YES;
        myCenterMapButton.hidden=YES;
    }
    if(mySegmentMenu.selectedSegmentIndex==2)
    {
        [self gotoHistoryViewController:sender];
    }
    [self.messagesController textFieldShouldReturn:nil];
    
}

- (IBAction)showMenu:(id)sender
{
    [self.popoverController presentPopoverFromBarButtonItem:self.myMenuButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(void)gotoHistoryViewController:(id)sender
{
    [self.myMessagesContainer endEditing:YES];
    [self performSegueWithIdentifier:@"HistoryView" sender:self];

    
    ///Обновление истории сообщений
    [self performSelector:@selector(updateHistory)
               withObject:nil
               afterDelay:1.0];
}

-(void)selectSubscriberWithSSI:(NSNumber*)ssi
{

#pragma mark TODO
    ///Возвращаем предыдущей ячейке не активную картинку

    ///На текущую ячейку ставим активную картинку
    [unreadMess setValue:0 forKey:[ssi stringValue]];
    myTableCell.noticeLabel.text=nil;

    //Послать контроллеру диалогов выбранный целеой SSI
    [self.messagesController selectTargetSubscriberSSI:ssi];
    
    //Отобразить окно с диалогом
    [self showDialog];
}


-(void)showDialog
{
    ///Отобразить окно с диалогом.
    self.myMessagesContainer.hidden=NO;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    myMessagesContainer.alpha = 1.0f;
    [UIView commitAnimations];
    
    self.myCenterMapButton.hidden=YES;
    self.mySlideNotificationsButton.hidden=YES;
    self.mySlideSubscribersButton.hidden=YES;
}
-(void)hideDialog
{
    ///Отобразить окно с диалогом.
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDelegate:self];
    myMessagesContainer.alpha = 0.0f;
    [UIView commitAnimations];
    self.myMessagesContainer.hidden=YES;
    
    self.myCenterMapButton.hidden=NO;
    self.mySlideNotificationsButton.hidden=NO;
    self.mySlideSubscribersButton.hidden=NO;
}


-(void)handleTapGesture:(UITapGestureRecognizer*)recognizer
{
    CGPoint p = [recognizer locationInView:self.view];
    
    CALayer *layerForHitTesting = self.mySlideNotificationsButton.layer;
	CALayer *hitLayer = [layerForHitTesting hitTest:p];
	if( hitLayer )
    {
        [self myNotificationsButtonClick:self];
        return;
    }
    
    layerForHitTesting = self.mySlideSubscribersButton.layer;
    hitLayer = [layerForHitTesting hitTest:p];
    if( hitLayer )
    {
        [self mySubscribersButtonClick:self];
        return;
    }
    
    layerForHitTesting = self.myCenterMapButton.layer;
    hitLayer = [layerForHitTesting hitTest:p];
    if( hitLayer )
    {
        [self centerMap:self];
        return;
    }
}

- (IBAction)mySubscribersButtonClick:(id)sender {
    if (isOpenSlideSubscribers) {
        [self hideSubscribersTable];
        isOpenSlideSubscribers=NO;
    }
    else
    {
        [self showSubscribersTable];
        isOpenSlideSubscribers=YES;
    }
}

- (IBAction)myNotificationsButtonClick:(id)sender {
    if (isOpenSlydeNotifications) {
        [self hideNotificationsTable];
        isOpenSlydeNotifications=NO;
    }
    else
    {
        [self showNotificationsTable];
        isOpenSlydeNotifications=YES;
    }
}

-(void)showSubscribersTable
{
    SVGKImage *hideSubscrImg=[SVGKImage imageNamed:@"show-hide3.svg"];

    [UIView beginAnimations:nil
                    context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5f];
    [self.mySubscribersTable setFrame:CGRectMake(0,64,180,704)];
    [self.mySlideSubscribersButton setFrame:CGRectMake(180-1, 348, 37, 73)];
    
    SVGKImageView* imgView= (SVGKImageView*)mySlideSubscribersButton.subviews.lastObject;
    [imgView setImage:hideSubscrImg];
    /*[self.mySlideSubscribersButton setImage:[UIImage imageNamed:@"hide.png"]
                                   forState:UIControlStateNormal];*/
    [UIView commitAnimations];
}
-(void)hideSubscribersTable
{
    SVGKImage *showSubscrImg=[SVGKImage imageNamed:@"show-hide1.svg"];

    [UIView beginAnimations:nil
                    context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5f];
    [self.mySubscribersTable setFrame:CGRectMake(-180, 64, 180, 704)];
    [self.mySlideSubscribersButton setFrame:CGRectMake(-1, 348, 37, 73)];
    
    SVGKImageView* imgView= (SVGKImageView*)mySlideSubscribersButton.subviews.lastObject;
    [imgView setImage:showSubscrImg];
    /*[self.mySlideSubscribersButton setImage:[UIImage imageNamed:@"show2.png"]
                                   forState:UIControlStateNormal];*/
    [UIView commitAnimations];
}
-(void)showNotificationsTable
{
    SVGKImage *hideNotifiImg=[SVGKImage imageNamed:@"show-hide4.svg"];

    [UIView beginAnimations:nil
                    context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5f];
    [self.myNotificationsTable setFrame:CGRectMake(844, 64, 180, 704)];
    [self.mySlideNotificationsButton setFrame:CGRectMake(808, 348, 37, 73)];
    
    SVGKImageView* imgView= (SVGKImageView*)mySlideNotificationsButton.subviews.lastObject;
    [imgView setImage:hideNotifiImg];
    /*[self.mySlideNotificationsButton setImage:[UIImage imageNamed:@"hide2.png"]
                                     forState:UIControlStateNormal];*/
    [self.myCenterMapButton setFrame:CGRectMake(796, 708, 40, 40)];
    [UIView commitAnimations];
}
-(void)hideNotificationsTable
{
    SVGKImage *showNotifiImg=[SVGKImage imageNamed:@"show-hide2.svg"];

    [UIView beginAnimations:nil
                    context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5f];
    [self.myNotificationsTable setFrame:CGRectMake(1024, 64, 180, 704)];
    [self.mySlideNotificationsButton setFrame:CGRectMake(988, 348, 37, 73)];
    
    SVGKImageView* imgView= (SVGKImageView*)mySlideNotificationsButton.subviews.lastObject;
    [imgView setImage:showNotifiImg];
    /*[self.mySlideNotificationsButton setImage:[UIImage imageNamed:@"show.png"]
                                     forState:UIControlStateNormal];*/
    [self.myCenterMapButton setFrame:CGRectMake(964, 708, 40, 40)];
    [UIView commitAnimations];
}

//подсчитать кол-во непррочитанных сообщений для SSI
-(void)checkUnreadMessForSSI:(NSNumber*)ssi
{
    NSString* keySSI=[ssi stringValue];
    SubscriberTableViewCell *cell=[self cellWithSSI:ssi];
    int counter=[[unreadMess valueForKey:keySSI] intValue];
    if (counter!=0)
    {
        NSString *unread=[NSString stringWithFormat:@"+%@", [[unreadMess valueForKey:keySSI] stringValue]];
        cell.noticeLabel.text=unread;
    }
    else
    {
        cell.noticeLabel.text=nil;
    }
}

//Получить ячейку  с указанным SSI
-(SubscriberTableViewCell*)cellWithSSI:(NSNumber*)ssi
{
    for (SubscriberTableViewCell* cell in mySubscribersTable.visibleCells)
    {
        NSNumberFormatter* formatter=[[NSNumberFormatter alloc] init];
        NSNumber *cellSSI=[formatter numberFromString:cell.ssiLabel.text];

        if([cellSSI longValue]==[ssi longValue])
        {
            return cell;
        }
    }
    return nil;
}





#pragma mark GPS methods
//Трекер положения устройства
- (void)locationManager:(CLLocationManager *)locationManager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;
{
    center=newLocation.coordinate;
    
    //передвигаем маркер на карте
    markerManager = [myMapView markerManager];
    RMLatLong latlong;
    latlong.latitude=center.latitude;
    latlong.longitude=center.longitude;
    [markerManager moveMarker:[markersDictionary valueForKey:[currentSubscriberSSI stringValue]]
                     AtLatLon:latlong];
    
    //сохранняем в базу
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Subscribers"];
    NSArray *subscrbs=[context executeFetchRequest:fetchRequest error:nil];
    NSNumber * longtitudeNumber = [[NSNumber alloc ]initWithDouble:latlong.longitude];
    NSNumber * latitudeNumber =[NSNumber numberWithDouble:latlong.latitude];
    for(NSManagedObject *obj in subscrbs)
    {
        if([[obj valueForKey:@"ssi"] longValue]==[currentSubscriberSSI longValue])
        {
            [obj setValue:longtitudeNumber forKey:@"longitude"];
            [obj setValue:latitudeNumber forKey:@"latitude"];
            break;
        }
    }
    NSError *error = nil;
    if(![context save:&error]){
        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
    }

    /*
    if (oldLocation!=nil)
    {
        CLLocationDistance distance=[oldLocation distanceFromLocation:newLocation];
        if (distance>100)
        {
            center=newLocation.coordinate;
        }
    }
    else
    {
        center=newLocation.coordinate;

        //передвигаем маркер на карте
        markerManager = [myMapView markerManager];
        RMLatLong latlong;
        latlong.latitude=center.latitude;
        latlong.longitude=center.longitude;
        [markerManager moveMarker:[markersDictionary valueForKey:[currentSubscriberSSI stringValue]]
                         AtLatLon:latlong];
        
        //сохранняем в базу
        NSManagedObjectContext *context = [self managedObjectContext];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Subscribers"];
        NSArray *subscrbs=[context executeFetchRequest:fetchRequest error:nil];
        NSNumber * longtitudeNumber = [[NSNumber alloc ]initWithDouble:latlong.longitude];
        NSNumber * latitudeNumber =[NSNumber numberWithDouble:latlong.latitude];
        for(NSManagedObject *obj in subscrbs)
        {
            if([[obj valueForKey:@"ssi"] longValue]==[currentSubscriberSSI longValue])
            {
                [obj setValue:longtitudeNumber forKey:@"longitude"];
                [obj setValue:latitudeNumber forKey:@"latitude"];
                break;
            }
        }
        NSError *error = nil;
        if(![context save:&error]){
            NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
        }

    }
     */
}


//Метод центрирования карты на текущем устрройстве
-(IBAction)centerMap:(id)sender
{
    [self localeToSubscriberWithSSI:[NSNumber numberWithLong:105]];
    /*
    //Метод для проверки достижимости серверов
    //[self updateConnectionsStatus];
     */
}

-(void)localeToSubscriberWithSSI:(NSNumber*)subscriberSSI
{
    RMMarker *myMarker=[markersDictionary valueForKey:[subscriberSSI stringValue]];
    
    CLLocationCoordinate2D loc=[markerManager latitudeLongitudeForMarker:myMarker];
    [myMapView moveToLatLong:loc];
    
    //Анимация маркера, дабы заметить
    myMarker.cornerRadius=30.0f;
    myMarker.borderColor=[UIColor redColor].CGColor;
    
    CABasicAnimation *markerAnimation;
    markerAnimation=[CABasicAnimation animationWithKeyPath:@"borderWidth"];
    markerAnimation.duration=0.5;
    markerAnimation.repeatCount=2;
    markerAnimation.autoreverses=NO;
    markerAnimation.fromValue=[NSNumber numberWithFloat:5.0f];
    markerAnimation.toValue=[NSNumber numberWithFloat:0.0f];
    [myMarker addAnimation:markerAnimation forKey:@"localeSubscriber"];
}


#pragma mark CoreData

-(void)InitSubscribers{
    selectedSubscribers=[self CoreDataBaseUpdate];
    markersDictionary=[[NSMutableDictionary alloc]init];
    unreadMess=[[NSMutableDictionary alloc] initWithCapacity:selectedSubscribers.count];
    
    //Добавленине маркеров абонентов
    markerManager = [myMapView markerManager];
    UIImage *redMarkerImage = [UIImage imageNamed:@"marker-red.png"];
    UIImage *blueMarkerImage = [UIImage imageNamed:@"marker.png"];
    //перебираем массив абонентов
    for (NSManagedObject *obj in selectedSubscribers)
    {
        NSLog(@"Name %@", [obj valueForKey:@"name"]);
        
        NSNumber *ssi=[obj valueForKey:@"ssi"];
        //Особый маркер для тображения нашего местоположения
        if ([ssi longValue]==[currentSubscriberSSI longValue])
        {
            newMarker = [[RMMarker alloc] initWithUIImage:blueMarkerImage anchorPoint:CGPointMake(0.5, 1)];
        }
        else
        {
            newMarker = [[RMMarker alloc] initWithUIImage:redMarkerImage anchorPoint:CGPointMake(0.5, 1.0)];
        }
        //Размещаем маркеры на карте
        center.longitude=[[obj valueForKey:@"longitude"] doubleValue];
        center.latitude=[[obj valueForKey:@"latitude"]   doubleValue];
        [newMarker changeLabelUsingText:[ssi stringValue]];
        [markersDictionary setValue:newMarker forKey:[ssi stringValue]];
        [markerManager addMarker:newMarker AtLatLong:center];
        //Инициализируем счетчик непрочитанных сообщений
        [unreadMess setObject:[NSNumber numberWithInt:0]
                       forKey:[ssi stringValue]];
    }
}


-(NSMutableArray *)CoreDataBaseUpdate
{
    NSLog(@"Checking database...");
    
    NSMutableArray *mySubcribers;
    
    ///Проверка есть ли данные в базе.
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Subscribers"];
    //Словари для хранения абонентов,маркеров абонентов, количества непрочитанных сообщений абонентов
    mySubcribers =[[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    //Следующая кострукция предназначена для добвления в
    BOOL isExist=NO;
    for(int i=0;i<ssiList.count;i++)
    {
        NSString *strSSI=[ssiList objectAtIndex:i];
        NSNumber *ssiNumer=[NSNumber numberWithInt:[strSSI intValue]];
        for (int j=0;j<mySubcribers.count; j++) {
            NSNumber *ssiNumer2=[[mySubcribers objectAtIndex:j] valueForKey:@"ssi"];
            if([ssiNumer longValue]==[ssiNumer2 longValue]) {
                isExist=YES;
                break;
            }
        }
        if (!isExist) {
            NSManagedObject *newSubscriber = [NSEntityDescription insertNewObjectForEntityForName:@"Subscribers" inManagedObjectContext:context];
            [newSubscriber setValue:ssiNumer forKey:@"ssi"];
        }
        isExist=NO;
    }
    
    NSError *error = nil;
    if(![context save:&error]){
        NSLog(@"Can't save! %@ %@", error, [error localizedDescription]);
        return nil;
    }
    else
    {
        NSLog(@"DB successfully created!");
        mySubcribers =[[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
        return mySubcribers;
    }
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark Reachability

-(void)updateConnectionsStatus
{
    SimplePingHelper *isolonaPinger=[[SimplePingHelper alloc] initWithAddress:@"10.255.253.170"
                                                                    target:self
                                                                       sel:@selector(isolonaServerState:)];
    [isolonaPinger go];
}

-(void)isolonaServerState:(NSNumber*)success
{
    NSDictionary *dic;
    SVGKImageView *isolonaServerStateImage=[connectionStateView.subviews objectAtIndex:1];
    BOOL reachable=[success boolValue];
    if (reachable)
    {
        if (!isOnlineServer)
        {
            isOnlineServer=YES;
            [isolonaServerStateImage setImage:[SVGKImage imageNamed:@"indicator4.svg"]];
            NSArray *obj=[[NSArray alloc]initWithObjects:[NSNumber numberWithInt:NotifiTypeSystemMessage],
                          NSLocalizedString(@"ONCONNECTION", nil),
                          nil];
            NSArray *key=[[NSArray alloc]initWithObjects:@"NotifiType",@"info", nil];
            dic=[[NSDictionary alloc]initWithObjects:obj forKeys:key];
            [self.notifiController addNotificationWithDictionary:dic];
        }
    }
    else
    {
        if (isOnlineServer)
        {
            isOnlineServer=NO;
            [isolonaServerStateImage setImage:[SVGKImage imageNamed:@"indicator3.svg"]];
            NSArray *obj=[[NSArray alloc]initWithObjects:[NSNumber numberWithInt:NotifiTypeSystemMessage],
                          NSLocalizedString(@"OFFCONNECTION", nil),
                          nil];
            NSArray *key=[[NSArray alloc]initWithObjects:@"NotifiType",@"info", nil];
            dic=[[NSDictionary alloc]initWithObjects:obj forKeys:key];
            [self.notifiController addNotificationWithDictionary:dic];
        }
    }
}


#pragma mark notifications

-(void) updateHistory
{
//    NSLog(@"update history");
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Message"];
    
    NSSortDescriptor *sortByDate = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    [fetchRequest setSortDescriptors:[[NSArray alloc]initWithObjects: sortByDate, nil]];
    
    NSMutableArray *subscrbs=[[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    ///Отправляем уведомление содержащее историю сообщений
    NSArray *objects=[[NSArray alloc]initWithObjects: subscrbs, nil];
    NSArray *keys=[[NSArray alloc]initWithObjects:@"messages", nil];
    NSDictionary *userInfo=[[NSDictionary alloc]initWithObjects:objects forKeys:keys];
    NSNotification *notificationObject=[NSNotification notificationWithName:@"showHistory"
                                                                     object:self
                                                                   userInfo:userInfo];
    [[NSNotificationCenter defaultCenter]postNotification:notificationObject];
}


@end
