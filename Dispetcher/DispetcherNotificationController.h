//
//  DispetcherNotificationController.h
//  Dispetcher
//
//  Created by Barbarossa on 15.08.14.
//
//

#import <UIKit/UIKit.h>

#import "SVGKit.h"
#import "SVGKImageView.h"
#import "SVGKFastImageView.h"
#import "SVGKLayeredImageView.h"

/**
 @memberof DispetcherNotificationController NotifiType тип появляющегося уведомления<br>
 <ul>
 <li>NotifiTypeTextMessage - Текстовое сообщение</li>
 <li>NotifiTypeSystemMessage - Системное сообщение</li>
 </ul>
 */
enum NotifiType
{
    NotifiTypeTextMessage=1,
    NotifiTypeSystemMessage=2
};

/**
 @brief Класс для контроля за отображением всплывающих уведомлений в боковом меню "Уведомления"
 @details Реализует набор методов и свойств следующих протоколов:
     <ul>
     <li>UITableViewDataSource,</li>
     <li>UITableViewDelegate,</li>
     </ul>
 */
@interface DispetcherNotificationController : NSObject <UITableViewDataSource,UITableViewDelegate>
{
    ///Идентификатор ячейки для уведомлений
    NSString *cellType;
    ///Массив, со держащий в себе объекты-уведомления
    NSMutableArray *notifiArr;
    ///Объект, содержищий в себе уведомление
    NSDictionary *notification;
    ///Табличное представление для отображения уведомлений
    UITableView *myTableView;
    ///Метка для отображения текста, сообщающего, что новых уведомлений уведомлений нет
    UILabel *noneNotifiLabel;
}

@property (strong,nonatomic) NSString *cellType;
@property (strong, nonatomic) UITableView *myTableView;


/**
 @function Функция, использующаяся для инициализации объекта класса табличный вид, в который и будут помещаться уведомления
 @param (UITableView*)tableView табличный вид, куда будут добавлятся уведомления
 @return Объект класса @ref DispetcherNotificationController
 */
-(id)initWithTableView:(UITableView*)tableView;
/**
 @method Метод размещающий уведомление в таблице, которую передали в @ref initWithTableView:
 @param (NSDictionary*)dictionary - словарь содежащий в себе информацию о уведомлении
 */
-(void)addNotificationWithDictionary:(NSDictionary*)dictionary;
@end
