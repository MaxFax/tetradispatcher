//
//  SoundSDS.h
//  Dispetcher
//
//  Created by Barbarossa on 13.08.14.
//
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioServices.h>

/**
 @brief Класс для воспроизведения звука соообщения
 */
@interface SoundSDS : NSObject
{
}
/**
 @method Статический метод воспроизведения звука по указанному названию файла
 @param (NSString*)soundName название воспроизводимого файла
 */
+(void)playSound:(NSString*)soundName;

@end
