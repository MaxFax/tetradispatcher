//
//  LayerMessage.m
//  Dispetcher
//
//  Created by Admin on 25.06.14.
//
//

#import "LayerMessage.h"

#if !__has_feature(objc_arc)
#error WToast requires ARC
#endif

#define TABBAR_OFFSET 44.0f

@implementation LayerMessage

- (id)initWithFrame:(CGRect)frame
{
    if((self = [super initWithFrame:frame])!=nil)
    {
        _duration=kWTShort;
        self.userInteractionEnabled=YES;
    }
    return self;
    
}

- (void)__show {
	__weak typeof(self) weakSelf = self;
	
	[UIView
	 animateWithDuration:0.2f
	 animations:^{
		 weakSelf.alpha = 1.0f;
	 }
	 completion:^(BOOL finished) {
		 [weakSelf performSelector:@selector(__hide) withObject:nil afterDelay:_duration];
	 }];
}

- (void)__hide {
	__weak typeof(self) weakSelf = self;
    
	[UIView
	 animateWithDuration:0.8f
	 animations:^{
		 weakSelf.alpha = 0.0f;
	 }
	 completion:^(BOOL finished) {
		 [weakSelf removeFromSuperview];
	 }];
}

+ (LayerMessage *)__createWithText:(NSString *)text {
	CGFloat screenWidth;
	CGSize screenSize = [UIScreen mainScreen].bounds.size;
	
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    switch (orientation) {
        case UIInterfaceOrientationPortraitUpsideDown: {
			screenWidth = MIN(screenSize.width, screenSize.height);
            break;
		}
        case UIInterfaceOrientationLandscapeLeft: {
            screenWidth = MIN(screenSize.width, screenSize.height);
            break;
		}
        case UIInterfaceOrientationLandscapeRight: {
			screenWidth = MIN(screenSize.width, screenSize.height);
            break;
		}
        default: {
            screenWidth = MIN(screenSize.width, screenSize.height);
            break;
		}
    }
	
	CGFloat x = 50.0f;
	CGFloat width = screenWidth - x * 2.0f;
    
	UILabel *textLabel = [[UILabel alloc] init];
	textLabel.backgroundColor = [UIColor clearColor];
	textLabel.textAlignment = NSTextAlignmentCenter;
	textLabel.font = [UIFont systemFontOfSize:14];
	textLabel.textColor =[UIColor colorWithRed:255 green:255 blue:255 alpha:1];
	textLabel.numberOfLines = 0;
	textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	CGRect tmpRect = CGRectZero;
    CGSize sizeConstraint = CGSizeMake(width - 20.0f, FLT_MAX);
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        CGSize textSize = [text sizeWithFont:textLabel.font
                           constrainedToSize:sizeConstraint
                               lineBreakMode:NSLineBreakByWordWrapping];
        tmpRect.size = textSize;
#pragma clang diagnostic pop
    }
    else {
        tmpRect = [text boundingRectWithSize:sizeConstraint
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName: textLabel.font}
                                     context:nil];
    }
	tmpRect.size.width = width;
	tmpRect.size.height = MAX(tmpRect.size.height + 20.0f, 38.0f);
    
	LayerMessage *toast = [[LayerMessage alloc] initWithFrame:tmpRect];
	toast.backgroundColor =[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8f];
	CALayer *layer = toast.layer;
	layer.masksToBounds = YES;
	layer.cornerRadius = 5.0f;
    
	textLabel.text = text;
    tmpRect.origin.x = 10;
	tmpRect.origin.y = 10;
	//tmpRect.origin.x = floor((toast.frame.size.width - tmpRect.size.width) / 12.0f);
	//tmpRect.origin.y = floor((toast.frame.size.height - tmpRect.size.height) / 12.0f);
	textLabel.frame = tmpRect;
    
	[toast addSubview:textLabel];
    
	toast.alpha = 0.0f;
    
	return toast;
}

+ (void)showWithText:(NSString *)text {
	[LayerMessage showWithText:text duration:kWTShort];
}

+ (void)showWithText:(NSString *)text duration:(RectangleMessageDuration)duration {
	LayerMessage *toast = [LayerMessage __createWithText:text];
	toast.duration = duration;
	
	UIWindow *mainWindow = [[UIApplication sharedApplication] keyWindow];
	[mainWindow addSubview:toast];
	[toast __show];
}


@end

