//
//  HistoryDetailsController.h
//  Dispetcher
//
//  Created by Barbarossa on 07.08.14.
//
//

#import <UIKit/UIKit.h>

/**
 @brief Контроллер всплывающего окна в @ref HistoryViewController
 @details Контроллер всплавающего окна, отобржающийся при нажатии пользователя по сообщению, где не влезает всеь текст целиком.
 */
@interface HistoryDetailsController : UIViewController
{
    UILabel *textOfMessage;
}

///Метка для отображения сообщения
@property (strong,nonatomic) UILabel *textOfMessage;
///Делегат контроллера всплывающих сообщений
@property(nonatomic,weak) UIPopoverController *popoverController;

/**
 @function Инициализатор контроллера
 @param (NSString *)text - текст сообщения
 @return объект класса @ref HistoryDetailsController
 */
- (id)initWithTextOfMessage:(NSString *)text;
@end
