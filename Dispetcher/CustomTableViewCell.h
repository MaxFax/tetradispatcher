//
//  CustomTableViewCell.h
//  Dispetcher
//
//  Created by Admin on 15.07.14.
//
//

#import <UIKit/UIKit.h>

/**
 @brief Пользовательский класс для отображения ячеек в таблице с историей сообщений
 */
@interface CustomTableViewCell : UITableViewCell
{
    ///Метка для отображения даты и времени прихода сообщения
    UILabel *date;
    ///Метка для отображения получателя сообщения
    UILabel *receiver;
    ///Метка для отображения отправителя сообщения
    UILabel *sender;
    ///Метка содержащая текст сообщения
    UILabel *text;
}

/**@defgroup cellProp Свойства для обращения к меткам ячейки
  @{
 */
///свойство для обращения к полю даты получения сообщения
@property (nonatomic, retain) IBOutlet UILabel *date;
///свойство Для обращения к полю получателя сообщения
@property (strong, nonatomic) IBOutlet UILabel *receiver;
///свойство Для обращения к полю отправителя сообщения
@property (nonatomic, retain) IBOutlet UILabel *sender;
///свойство Для обращения к полю с текстом сообщения
@property (nonatomic, retain) IBOutlet UILabel *text;
///@}
@end
